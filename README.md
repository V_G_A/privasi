# PrivaSì

> ! Questo è il backend del progetto, fatto per chi vuole contribuire alla sua stesura. La versione ufficiale (e facilmente consultabile) si trova invece presso https://privasi.eticadigitale.org

Se pensi di non aver nulla da nascondere, sei nel posto giusto: PrivaSì, infatti, nasce per analizzare il discorso privacy da più punti di vista. Non solo quello di sicurezza, bensì anche quello etico, più umano. Per esempio, la domanda che forse dovresti porti non è "cosa mi importa se tanto non ho nulla da nascondere?" ma "perché dovrei condividere con una macchina cose che, se sommate, non direi neanche alla persona più cara?".

Attraverso un percorso passo dopo passo, PrivaSì si propone di rispondere a questo e altri quesiti mentre guida la persona nell'utilizzo di mezzi più etici, non invasivi, accompagnandola dall'installazione fino ad eventuali modifiche. Ci si tiene a sottolineare che *i quesiti* sono il fattore chiave, perché seguire istruzioni senza capirne il senso avrebbe sul lungo corso un effetto controproducente. Ed è proprio per questo che le spiegazioni vengono sempre *prima* della guida vera e propria, utilizzando un linguaggio semplice per essere compreso da tutte le età. Capire, prima di fare.  

Si ricorda inoltre che il progetto è sotto licenza libera, ovvero chiunque è libero di contribuire alla sua stesura, crearne una propria versione e/o ridistribuirla.  
Per ulteriori domande, consultate le [FAQ](https://privasi.eticadigitale.org/faq/) (domande più frequenti).

[INIZIA IL PERCORSO](https://privasi.eticadigitale.org)  
<br>  
  
### Contatti

[Sito](https://eticadigitale.org/)  
[Canale Telegram](https://t.me/eticadigitalechannel)  
[Gruppo Telegram](https://t.me/EticaDigitale)  
mail: etica.digitale@mailfence.com
