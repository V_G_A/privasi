---

#title: "Non nel mio nome pt.2: browser, finestre della rete"
weight: 13

---

# Non nel mio nome pt.2: Browser, finestre della rete

> Google Chrome è il browser più usato al mondo, scaricato 5 miliardi di volte sui soli telefoni[^1]. Quanto rispetta la nostra sfera privata? Purtroppo ben poco

## In parole semplici

Si immagini di guardare fuori da una finestra. Delle montagne si stagliano all'orizzonte, una distesa verde si sviluppa ai loro piedi. E scende, dolcemente scende fino a incrociare il passaggio di un ruscello, che le stagioni hanno adornato con... una macchia sul vetro?  

C'è un bel paesaggio fuori, ma la macchia sul vetro ha interrotto la nostra immersione. Perché la finestra è il nostro filtro sull'esterno, e quello che vediamo dipende da lei.  

Anche il mondo digitale ha delle finestre, e le chiamiamo browser (Chrome, Firefox, Edge ecc.): se ci pensiamo, per aprire un sito internet passiamo dal browser. Quasi tutti i posti in cui navighiamo, gli spazi online in cui scriviamo, passano dal browser - una vera e propria finestra sul digitale.  

Finora ci siamo concentrati su ciò che c'è oltre la finestra, e l'abbiamo fatto senza mai creare nuove cose (profili, mail ecc). E lo abbiamo fatto per un semplice motivo: se la finestra è compromessa, prima va cambiata la finestra.  

Per capire quanto questo singolo punto d'accesso alla rete possa diventare problematico, un giornalista del Washington Post aveva analizzato nel 2019 il comportamento di Google Chrome e quello di Mozilla Firefox. Mettendoli a confronto dopo una settimana, Chrome aveva autorizzato più di *undicimila* richieste di cookie, mentre Firefox zero[^2]. In altre parole, nel primo caso il giornalista era stato costantemente profilato.  

Nel livello precedente ci siamo occupati di installare una finestra rispettosa della nostra intimità. Quello che faremo in questo capitolo, invece, è migliorarne la riservatezza, installando delle estensioni che andranno a fungere da tende digitali. Tende che, in sintesi, non lasceranno molto da vedere ai siti che vorranno guardare dentro.

## Cosa fare

### Smartphone 

#### Brave Browser

Il nostro consiglio è di sostituire il browser che state usando dal vostro smartphone con Brave: un browser indipendente, sicuro e rispettoso della privacy. Per farlo, andate sullo Store, cercate Brave, installatelo e impostatelo come browser predefinito.

Aprite il browser > ⋮ tre puntini in basso > ⚙️ Impostazioni > Display > Aspetto > togliete la spunta a *Icona di Ricompense Brave* (a meno che non vi interessino le criptovalute). Tornate indietro e nella sezione Display > Nuova Pagina > togliete la spunta a *Mostra immagini sponsorizzate*

Brave vi proteggera dai traccianti grazie alla sua funzionalità integrata chiamata "Scudo di Brave". Inoltre il motore di ricerca predefinito di Brave è Brave Search, più privato di Google[^3], ma potete scegliere il vostro preferito leggendo [il nostro capitolo](/percorso/l1-5___search-engine/)


#### DuckDuckGo Browser
Se non vi piace Brave, allora vi consigliamo di provare Duck Duck Go, potete scaricarlo da F-Droid, il negozio alternativo che vi abbiamo fatto scaricare nello scorso capitolo.

### PC: Installazione e impostazioni base

Qui le cose si fanno più impegnative, ma non scoraggiatevi. Tuttavia, finché non avete completato TUTTI i passaggi, NON usate Firefox e continuate col vostro vecchio browser.  
  
Prima di tutto scarichiamo e installiamo Firefox dal sito ufficiale: https://www.mozilla.org/it/firefox/new/  
Fatto ciò, apriamolo e andiamo nelle Opzioni. Ci sono due modi per farlo:
- premere sulle 3 lineette in alto a destra e, dal menù a scomparsa che si apre, su "Opzioni"
- scrivere ```about:preferences```  nella barra di ricerca e premere invio

Selezionate sulla sinistra "Privacy e sicurezza" e impostate quanto segue:
- Blocco Contenuti: restrittivo (blocca più traccianti possibili di base)
- Cronologia: in "Impostazioni cronologia" selezionate "utilizza impostazioni personalizzate" e spuntate "Utilizza sempre la modalità Navigazione Anonima" (vi chiederà di riavviare Firefox, fatelo)

*Se il PC è solo vostro*, tornate nelle opzioni e in "Avvio" premete su "Imposta come browser predefinito". Vi porterà alle impostazioni del computer e da lì sostituite Edge/Chrome con Firefox. Abbiamo detto SE è solo vostro, perché dare una doccia fredda di "Tiè, ora fai come faccio io" alle altre persone non è proprio la migliore delle soluzioni (ricordate che voi in primis non siete arrivati qui da un giorno all'altro).  

  
### Estensioni  

La finestra c'è, ma avventurarsi per internet non è ancora il massimo. Prima di aprire qualsiasi sito, è fondamentale mettere le tende. Apriamo Firefox e andiamo nel gestore delle estensioni di Mozilla: https://addons.mozilla.org/it/firefox/.  

#### uBlock Origin

Per cercare un'estensione, vi basta digitare il suo nome nella barra in alto a destra come illustrato qui sotto. Iniziamo da uBlock Origin

![L2-2_gif0.gif](../../images/L2-2_gif0.gif)


Parlando di docce fredde, questa è probabilmente la doccia del capitolo: con uBlock Origin andremo nelle impostazioni ad attivare vari filtri aggiuntivi e ad abilitare delle protezioni aggiuntive che ci salveranno da alcune rogne di internet.
  
Durante l'installazione ogni estensione vi chiederà se volete attivarla anche per la modalità in incognito: mettete sempre la spunta di conferma o non funzionerà.
  
![L2-2_pic3.png](../../images/L2-2_pic3.png)

Una volta installata, l'obiettivo è corazzarla adeguatamente abilitando alcune impostazioni e filtri opzionali. Prima di tutto, clicchiamo sull'icona di uBlock Origin nella parte in alto a destra del browser, dove compaiono le estensioni installate. Nella finestrella appena comparsa andiamo a cliccare sulle rotelle in basso a destra (`Dashboard`), ci compariranno davanti le opzioni di uBlock Origin. Assicuratevi dunque che abbiate spuntato tutto nella sezione privacy.

![L2-2_gif1.gif](../../images/L2-2_gif1.gif)

Ora attiveremo i filtri. Recatevi in `Filtri di terze parti` cliccando in alto nella pagina e seguite la GIF per sapere quali abilitare:

![L2-2_gif2.gif](../../images/L2-2_gif2.gif)

Una volta cliccato sulle caselle, ricordatevi di premere su `Aggiorna ora` come vedete nella GIF.

Un ultimo passo prima di concludere questa sezione è aggiungere Actually Legitimate URL Shortener Tool, uno strumento che fa da spazzino per i traccianti presenti nei collegamenti dei siti.

uBlock Origin > Cliccate sugli ingranaggi > I miei filtri > Copiate questo link https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt e incollatelo > Salva Cambiamenti > Finito! Potete chiudere questa finestra.


Bene, se siete sopravvissuti fin qui, il resto è una passeggiata.


#### CanvasBlocker

CanvasBlocker evita il  "cookie 2.0", il *fingerprinting* (letteralmente "prendere le impronte digitali"). Questo sistema al posto di lasciarvi i cookie nel browser, traccia le caratteristiche dell'intera finestra (come se appunto ne prendesse le impronte). Quando un sito ci prova, CanvasBlocker gli ritorna un dato fasullo. Ops :)  
Vi basta installarla come avete fatto per uBlock Origin


### Configurazione di Firefox

#### Autocancellazione dei cookie

Volete buttare via i [contenitori](/percorso/l2-0___cookies/) appena uscite dal negozio? Firefox vi consente di eliminare i cookie all'uscita dell'applicazione. Per attivare questa funzionalità:

Premete sulle tre lineette in alto > ⚙️ Impostazioni > Cerca nelle impostazioni e scrivete "Cookie e dati dei siti web" > Spuntate *Elimina cookie e dati dei siti web alla chiusura di Firefox*. Vi consigliamo di aggiungere delle eccezioni in questa schermata per tutti quei siti in cui il cookie serve per mantenere l'accesso e non per profilarvi, per esempio potreste aggiungere *https://web.telegram.org/a/* per Telegram Web.

> **ATTENZIONE**: non tenete aperti più siti dove accettate i cookie e/o con uBlock Origin disabilitato, o sarà possibile a quei siti fare 2+2. **Prendete l'abitudine di chiudere le schede quando non vi servono più!**

#### Modalità solo HTTPS

Avrete sicuramente notato all'inizio degli indirizzi le sigle HTTP e HTTPS. Queste sigle hanno il compito di metterci in contatto con i siti, come un vero e proprio ponte. La differenza tra le due è che HTTPS è più sicura (la S sta per *Secure*, sicuro in inglese), perché quello che fa è cifrare la comunicazione. Questa cosa è molto importante quando andiamo, per esempio, a immettere dati in una pagina web, perché se sarà HTTPS vorrà dire che chi sta dall'altro lato non potrà leggere in chiaro i dati inseriti.  
Molti siti dispongono di entrambe le versioni, ma è possibile forzare solo HTTPS dalle impostazioni di firefox:

Tre lineette in alto > ⚙️ Impostazioni > Cerca nelle impostazioni e scrivete "Modalità solo HTTPS" > Spunta su *Attiva in tutte le finestre*


## Ricapitolando...

La vera sfida del capitolo è abituarsi un po' al tutto. Perché ovviamente, se fate il login su un sito e Firefox vi cancella il cookie di accesso appena lo chiudete, ogni volta dovrete riloggare. Inizialmente può essere scomodo, ma sulla lunga può per esempio aiutarvi a rendervi conto di quali siti vi importa davvero e quali aprite per noia. Vi consigliamo di aggiungere tra le eccezioni solo i siti veramente importanti. 

Qualcuno invece potrebbe pensare che è assurdo fare "tutti" questi salti mortali (come uBlock Origin), ma vi invitiamo a riflettere sul fatto che questi salti mortali sono stati resi necessari proprio per come internet funziona attualmente, ovvero un'enorme macchina di pubblicità e dati. Ciò che dovrebbe indignare quindi, non è la "pretesa" a farvi installare questo e quest'altro anche solo per aprire un link, bensì il come le grandi compagnie di internet che circondano le nostre vite (Google, Facebook, Amazon, Twitter, Reddit e via dicendo) ci abbiano costretti a simili contromisure per non essere costantemente monitorati per i loro profitti. Vi dà fastidio? Dovrebbe. Ma prendetevela con loro, non con noi.

[Torna al percorso](../../indice)
  
## Appendice

[^1]: Verma Kashish, [Google Chrome pasts 5 billion downloads; a huge lead over Mozilla Firefox](https://thegeekherald.com/p/google-chrome-pasts-5-billion-downloads-a-huge-lead-over-mozilla-firefox/), The Geek Herald, 30 giugno 2019  
[^2]: Fowler Geoffrey A., [Goodbye, Chrome: Google’s Web browser has become spy software](https://www.washingtonpost.com/technology/2019/06/21/google-chrome-has-become-surveillance-software-its-time-switch/), The Washington Post, 21 giugno 2019  
[^3]: [How is Brave Search different? What does "independent" mean?](https://brave.com/search/#independent)
[^4]: Quaranta Davide, [CDN: cosa sono e come funzionano](https://informaticabrutta.it/cdn-content-delivery-networks/), Informatica Brutta, 9 agosto 2015 
