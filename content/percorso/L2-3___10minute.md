---
#Title: "Mail temporanee"
weight: 14

---

# Mail temporanee

> Volete iscrivervi a un sito senza che questo vi riempia poi la casella di posta? Detto fatto  

## In parole semplici

Dall'ultimo capitolo del primo livello dovreste aver capito una cosa: iscriversi a un sito è facilissimo, cancellarsi può diventare un incubo. Quindi perché rischiare di farsi un profilo su un qualsiasi sito quando possiamo prima andare in avanscoperta, senza il rischio di trovarci bloccati nell'ennesima corrispondenza mail per rimuoverlo?  

Il sito 10 Minute Mail nasce proprio per questo: creare una casella di posta temporanea dalla durata di 10 minuti. Ci viene fornito un indirizzo mail da usare come vogliamo e uno spazio per ricevere mail, ovvero il materiale necessario per iscriversi a un sito.  

Se volessimo poi continuare a usare il profilo sul sito in questione, è consigliato (se ne siete in grado) di andare nel profilo e modificare la mail con quella vostra vera e propria, spazzatura o meno; o in caso contrario di crearvi semplicemente un nuovo profilo (con la mail vera e propria, spazzatura o meno). Inoltre, alcuni siti si sono ingegnati e riconoscono quando le mail inserite provengono da siti come 10minutemail, anche se parliamo di una fetta davvero piccola: in quel caso sta a voi decidere se non registrarvi proprio o se farlo con una mail vera e propria.  

## Cosa fare

Esistono più versioni, noi useremo https://10minutemail.net/ per un semplice motivo: al contrario di quella più famosa, funziona anche senza javascript (una cosa che vedremo più avanti).  

Al centro trovate l'indirizzo di posta, mentre sotto 3 opzioni molto comode:
- **Ricarica pagina**: usatela per vedere se vi è arrivata una nuova mail
- **Dammi altri 10 minuti**: allunga la durata, fino a un massimo di 100 minuti
- **Dammi un altro indirizzo e-mail**: annulla l'attuale e ne genera uno nuovo

Infine, ancora più sotto trovate la casella di posta dove riceverete le mail.

[Torna al percorso](../../indice)  
