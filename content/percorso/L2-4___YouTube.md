---
#Title: Video in un riflesso
weight: 15

---

# ~~Youtube~~ Video in un riflesso

> C'è un modo per evitare la profilazione di YouTube? E quant'è cambiata la piattaforma in questi anni? Alla scoperta degli specchi

## In parole semplici

Vi abbiamo già raccontato pienamente di come funzioni Google (e YouTube appartiene a Google dal 2006), quindi dibattere per l'ennesima volta sulla privacy e i suoi modi per fare soldi ci pare superfluo. Parliamo, invece, semplicemente di YouTube.  

Il sito fu creato nel 2005 da tre impiegati PayPal e quello che offriva era semplice: uno spazio per condividere video. Non c'erano i pollici per votare, bensì le stelline (da 1 a 5), la gente si arrangiava con ciò che aveva in casa e fare lo "youtuber" come lavoro non era neanche immaginabile. La gente lo faceva per il gusto di fare, per esprimersi, per mettersi in contatto con altre persone, ma ancora di più per costruire una realtà più "personale" che si distaccasse dalla TV.  

Con il passare del tempo, YouTube è inevitabilmente cambiato. Sono emersi gli scopritori di talenti, i manager, fino a che YouTube non è diventato in alcuni casi una piattaforma di lancio per finire proprio in TV o sul grande schermo[^1]. Da qui il sito ha iniziato a prediligere i canali di emittenti televisive, scordandosi dell'utenza che lo aveva reso ciò che era: sono noti più casi dove la piattaforma ha trattato con due pesi due misure emittenti e youtuber che parlavano del medesimo argomento, demonetizzando questi ultimi: per esempio, quando il conduttore del Jimmy Kimmel Live (spettacolo televisivo della ABC) parlò delle sparatoria avvenuta a Las Vegas nel 2017 andò tutto bene[^2], ma quando ne parlò lo YouTuber Casey Neistat aprendo addirittura un fondo per le vittime, venne privato da YouTube della possibilità di guadagnare soldi su quel video[^3].  

Ulteriore testimonianza di questo risvolto sono gli ultimi YouTube Rewind: i Rewind sono video che dovrebbero riassumere gli eventi *clou* successi in un anno sulla piattaforma, ma la comunità si è sentita sempre meno rappresentata man mano che apparivano personaggi della TV come Will Smith, John Oliver e Trevor Noah[^4]. Inoltre, come evidenziato dagli youtuber RackaRacka, YouTube è diventato sì "disneyano", ma al tempo stesso produce spettacoli come Wayne che vanno contro tutte le politiche che tanto vuol fare rispettare a suon di demonetizzazione[^5] (scene violente ed esplicite, imprecazioni continue).

Quest'arma che YouTube usa, la demonetizzazione (ovvero non permettere di guadagnare soldi su un determinato video), è diventata un incubo per molti creatori e creatrici di contenuti, che hanno timore di parlare di certi argomenti per paura di farla scattare. Una volta demonetizzato un video *o addirittura l'intero canale*, YouTube non dà mai una motivazione precisa se non che "ha violato gli standard della community". È come venire accusati di un reato ma senza sapere quale.  

Nel corso degli anni YouTube è diventato esattamente quell'ambiente dal quale si voleva scappare: la TV e la sua censura (seppur più morbida). Che collabora con la TV e che produce i suoi contenuti originali, trattando con poco riguardo (e ingiustizia) quelle persone che hanno contribuito a far diventare popolare la piattaforma. Quest'ultime, per arrangiarsi, hanno aperto donazioni su siti esterni proprio per evitare le ansie della demonetizzazione, come Patreon o Ko-Fi. Ed è infatti diventato più efficiente supportarle da lì che con una visualizzazione.  

Cos'è quindi più etico fare? Venire profilati e contribuire a un sistema che non ricompensa neanche più i suoi creatori o continuarne egoisticamente ad usufruirne evitando sia la profilazione sia il supportare queste persone che faticano sempre di più? La risposta sul lungo corso è costruire delle alternative per far sì che questi creatori abbandonino YouTube, tuttavia quello che cerchiamo qui è una risposta più rapida (tratteremo piattaforme alternative più avanti).  

Quello che possiamo fare già da oggi e senza il minimo sforzo è guardare i video di YouTube da degli "specchi", siti che specchiano i contenuti di YouTube ma che (almeno quelli di cui parleremo) li privano di ogni mezzo di profilazione.  

## Cosa fare

#### NewPipe

NewPipe è un'app disponibile su F-Droid. La schermata principale si suddivide in Tendenze, Iscrizioni e Playlist salvate, e offre addirittura più funzionalità di YouTube stesso. Per esempio, potete riprodurre un video in sottofondo senza dover tenere lo schermo acceso (come una canzone). Per farlo vi basta tenere premuto su qualsiasi video e selezionare "Riproduci in sottofondo".  

Come YouTube, inoltre, permette di iscriversi ai canali; e non è richiesto nessun profilo! Se siete pratici e siete iscritti a tanti canali, potete esportare da YouTube le vostre iscrizioni come spiegato [qui](https://www.addictivetips.com/web/export-youtube-subscriptions/) e importare il file nella sezione "Iscrizioni" dell'app, sennò cercate semplicemente i vari canali da NewPipe e iscrivetevi da lì.  

Unica cosa da fare presente è che in nessuno degli specchi vi sarà permesso mettere mi piace e commentare, per un semplice motivo: sono funzioni che richiedono per forza un profilo YouTube. Esclusi i "mi piace", potreste darvi alla corrispondenza via mail con il vostro YouTuber preferito (e apprezzerà di più perché è più "diretto"). O, se proprio avete bisogno di commentare, aprite il video da YouTube e aggiungete il commento.  

NewPipe apre sia gli indirizzi YouTube che Invidious (spiegato qui sotto), ma anche SoundCloud, Bandcamp e PeerTube.

#### Invidious
Invidious è un software libero "ospitabile in casa", ottimo per vedere un video da browser senza passare per YouTube. Cosa si intende con ospitabile in casa? Si intende che non esiste un sito centrale dove tutti vengono indirizzati; al contrario, come tanti pianetini simili, ci sono tante versioni in giro per internet, tenete su da singoli individui che le rendono disponibili per tutti. Questo per 1) non far vertere il traffico su un unico pianetino, e 2) perché nel caso uno non vada, c'è sempre quello di riserva.

Chiamiamo questi pianetini, *istanze*. Se ci avete fatto caso, i video nell'appendice di questo capitolo (e in generale in tutto il percorso) non portano a YouTube ma fanno utilizzo di un'istanza raggiungibile al dominio https://yewtu.be. E la cosa bella è che qualsiasi indirizzo YouTube è facilmente convertibile in un indirizzo Invidious: basta togliere "youtu.be", "youtube.com" o "m.youtube.com" dal collegamento e sostituirlo con il dominio di un'istanza, come appunto "yewtu.be". Per esempio: 

```
https://youtube.com/watch?v=PuQt9N4Dsok  ==>  https://yewtu.be/watch?v=PuQt9N4Dsok (cliccare per credere) 
```

Quindi se ricevete un collegamento a YouTube da PC, vi basta applicare questo trucchetto per vederlo su qualche istanza di Invidious :) Inoltre se volete scaricare un video, vi basta far clic destro su di esso e "Salva video". L'unica apparente limitazione è che di base il sito non permette video di qualità più alta di 720p (se son più alti li scala in automatico) e che canali certificati come VEVO o i "Topic" di YouTube non riescono a venire riprodotti. Tuttavia per rimediare a questi due problemi è sufficiente aggiungere ```&quality=dash``` in fondo all’URL del video. Esempio: 

```
https://yewtu.be/watch?v=PuQt9N4Dsok  ==>  https://yewtu.be/watch?v=PuQt9N4Dsok&quality=dash 
```

L'unica pecca è che in questo caso il video si caricherà più lentamente. Per una lista delle istanze potete consultare https://invidio.us.  

#### FreeTube

FreeTube è un'applicazione desktop (PC) che usa la struttura di Invidious, ma senza limitazioni sulla qualità dei video e sulla riproduzione (come VEVO). Ed essendo un'applicazione, potete lanciare tutto dal computer senza aprire alcun browser. È disponibile per Windows, Mac e per molte distribuzioni Linux. Potete scaricarlo da [QUI](https://freetubeapp.io/#download).  

Come NewPipe, anche FreeTube permette le iscrizioni, che potete aggiungere sia manualmente che importandole da YouTube. E che, al contrario di NewPipe, vengono visualizzate proprio come su YouTube. Il design, inoltre, è stato pensato simile a YouTube per permettere una migliore transizione da uno all'altro. L'unica sua limitazione attuale è che non permette di visualizzare più di 20 commenti per video. Tra le tre, è senza ombra di dubbio la più completa.  

Complimenti, avete appena ridotto il vostro consumo di YouTube se non del 100%, almeno del 90% :D  

Come molte applicazioni libere, vi ricordiamo infine che donare agli sviluppatori - o aiutarli col codice se ne siete in grado - è un bel gesto, perché se non fosse per loro non avremmo queste alternative. Prendetevi un attimo e rifletteteci, perché anche un euro al mese fa la differenza (e se considerate che Spotify costa 10€/mese, quando potete ottenere una cosa simile con NewPipe creandovi delle playlist...).  
[Dona a NewPipe](https://liberapay.com/TeamNewPipe/) --- [Dona a Invidious](https://liberapay.com/omarroth) ---  [Dona a FreeTube](https://liberapay.com/FreeTube)

[Torna al percorso](../../indice)  

## Appendice
[^1]: come lo spiacevole [Game Therapy](https://www.imdb.com/title/tt4421344/)  
[^2]: Jimmy Kimmel Live, [Jimmy Kimmel on Mass Shooting in Las Vegas](https://yewtu.be/watch?v=ruYeBXudsds) (video), 2017  
[^3]: CaseyNeistat, [LET'S HELP THE VICTIMS OF THE LAS VEGAS ATTACK](https://yewtu.be/watch?v=tfZvSS_f254) (video), 2017  
[^4]: YouTube, [YouTube Rewind 2018: Everyone Controls Rewind | #YouTubeRewind](https://yewtu.be/watch?v=YbJOTdZBX1g) (video), 2018  
[^5]: RackaRacka, [Youtube hates RackaRacka #freeracka](https://yewtu.be/watch?v=q3blsxaw6bg) (video), 2019  
