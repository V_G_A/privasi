---
#Title: "Non nel mio nome pt.1: F-Droid, il negozio trasparente"
weight: 12

---

# Non nel mio nome pt.1: F-Droid, il negozio trasparente

> Se pensavate che Play Store e Apple Store fossero gli unici posti dove scaricare applicazioni, è tempo di ricredervi: F-Droid nasce con l'intento di promuovere app libere e senza traccianti

## In parole semplici

Prima di avventurarsi tra i vari siti e i loro cookie, è buona cosa passare per le app che abbiamo sul telefono - e lo faremo con F-Droid. F-Droid è un'app/portale che nasce con l'intento di fornire solo applicazioni libere, gratuite e sicure, con un controllo meticoloso da parte di chi le fornisce.  

Tramite un sistema di avvertenze visibile sotto ogni app, ci è poi possibile sapere se un'app ha quelle che il portale definisce "anti-caratteristiche"[^1]. Per esempio, potrebbe essere un'app che contiene pubblicità, traccianti o che promuove servizi non liberi, tuttavia tale sistema ci permette di fare una scelta informata (per i traccianti ci dirà "Questa app monitora e riferisce sulle tue attività").  
![L2-1_pic0.png](../../images/L2-1_pic0.png)

In futuro useremo F-Droid per scaricare alcune app (non disponibili nei normali negozi) in sostituzione di altre app a noi sì più familiari, ma -come dimostreremo- invasive. Questo ci garantirà maggior trasparenza su cosa stiamo scaricando, minor dipendenza dai grandi servizi opachi come il Play Store, e di conseguenza un telefono esposto a meno rischi.   

## Cosa fare

### Installazione

Dato che F-Droid rappresenta la concorrenza, non troverete l'app nei classici negozi. Per scaricarla dovete andare dal vostro telefono su https://f-droid.org/ e premere il tasto "Scarica F-Droid" ben visibile al centro.  
  
Una volta scaricata, il vostro telefono vi chiederà probabilmente se volete autorizzare il browser che avete usato (tipo DuckDuckGo) a installare app. Accettate e installate F-Droid, apritelo e aspettate che venga aggiornata la sua banca dati.
  

### Telegram, ma libero

Adesso possiamo andare alla ricerca di Telegram, che non è però lo stesso Telegram che già avete sul telefono: la versione libera su F-Droid si distingue perché le sono state rimosse le dipendenze da servizi proprietari (ovvero non liberi, ovvero, in questo caso, da Google[^2]).

Quindi molto semplicemente, disinstallate Telegram dal telefono (i messaggi rimangono, non preoccupatevi) e reinstallatelo cercandolo su F-Droid. La prima volta che provate ad installare un applicazione, vi verrà chiesto di dare i permessi a F-Droid di poter scaricare altre applicazioni. Talvolta alcune versioni di Android hanno interfacce un po' allarmistiche con lo scopo di farvi usare la testa prima di fidarvi di applicazioni sconosiute. Nel caso di F-Droid, non c'è nessun problema quindi date pure i permessi.

È giusto specificare che gli aggiornamenti rispetto all'app ufficiale arrivano con qualche giorno di ritardo: questo perché i volontari che lavorano sulla versione libera devono 1) aspettare che venga rilasciata la versione ufficiale, 2) avere il tempo per ripulirla. E anzi, dato che sono volontari, ricordatevi che supportare queste persone è un gesto molto importante e molto apprezzato.


### Dimmi che app sei e ti dirò quanti traccianti hai

Un'altra app molto interessante che vi consigliamo è **ClassyShark3xodus** (ha l'icona di uno squalo). Il suo ruolo è dirvi quanti traccianti ha un'app e usarla è facilissimo: vi basta aprirla e selezionare, dalla lista che vi apparirà, l'app che volete analizzare; farà tutto in automatico. Vi consigliamo quest'esercizio per rendervi conto di quanti traccianti risiedano nelle app che usiamo ogni giorno: eccovi un esempio sull'app di noleggio bici MoBike.  

![L2-1_pic2.png](../../images/L2-1_pic2.png)

Abbiamo evidenziato in verde il nome dell'app, in azzurro i traccianti trovati (4) e in arancione quali: in questo caso sappiamo che l'app si appoggia non solo ai servizi pubblicitari di Google (CrashLytics e Firebase Analytics) ma usa anche traccianti di Baidu, il "Google" cinese famoso per la censura a contenuti come Piazza Tiananmen 1989. Insomma: ecologico certo, ma etico...  


A questo punto quindi, avete 2 negozi sul telefono: uno ufficiale, per potervi comunque concedere qualsiasi sfizio, e un altro libero, con ovviamente molta meno scelta ma al tempo stesso del tutto trasparente per andare sul sicuro. E se proprio volete capire la trasparenza di un'app scaricata dal negozio ufficiale, vi basta controllarla con lo squalo di fiducia (che non vi dirà esattamente cosa fa, ma almeno chi e cosa traccia).  Vi lasciamo pasticciare un po' :)  

[Torna al percorso](../../indice) 

## Appendice
[^1]: https://f-droid.org/it/docs/Anti-Features/ 
[^2]: Per esempio il GCM (Google Cloud Messaging) che comunica a Google quando ricevete un messaggio, o la rimozione di Google Maps per condividere la posizione, sostituito da OpenStreetMap  
