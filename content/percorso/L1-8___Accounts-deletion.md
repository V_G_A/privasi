---
#Title: "Fuga dal superfluo"
weight: 10
---

# Fuga dal superfluo

> Quante parti di noi abbiamo lasciato fino a questo momento su internet, tra una registrazione e l'altra su siti di cui non ricordavamo neanche l'esistenza? Poco importa, perché è tempo di riprendercele

## In parole semplici  

Intanto, se siete arrivati fin qui seguendo ogni capitolo, ecco un riassunto dei progressi fatti:  
*  Avete detto a Google di farsi un po' più gli affari suoi
*  Avete aumentato la sicurezza dei vostri profili senza bisogno di avere password come 9sdA1l4#[F9d]ù0"klf_fka1£$L@àf
*  Avete detto a Facebook di farsi un po' di più gli affari suoi
*  Avete rimosso dalla vostra abitazione dispositivi di sorveglianza spacciati per amici intimi
*  Avete imparato a rivalutare il valore delle conversazioni con le persone a cui tenete
*  Avete imparato che molti dei siti che usiamo ogni giorno ci intrappolano in camere dell'eco per farci credere di aver sempre ragione. E come evitarlo per i motori di ricerca
*  Avete risparmiato soldi su licenze Office (e detto a Microsoft di farsi un po' più gli affari suoi)
*  Avete visto come piccoli gesti possano fare la differenza

È importante rendersi conto che non sono azioni da poco, soprattutto in preparazione dell'ultimo capitolo di questa sezione - che richiede tempo e può risultare provante.  

Se avete presente Marie Kondō (la scrittrice giapponese diventata famosa per il suo metodo di riordinare casa e migliorare la qualità della propria vita[^1]), in quest'ultima istanza andremo a fare essenzialmente la stessa cosa. Solo che, al posto di immergerci nel mondo fisico, lo faremo in quello digitale; ripulendolo dai nostri profili superflui o che ci eravamo dimenticati di avere.

Ogni volta che ci iscriviamo a un sito o a un'app, lasciamo una traccia indelebile del nostro passaggio sotto forma di dati. Lo facciamo senza accorgercene, magari registrandoci in cambio di coupon, di documenti per studiare, di una newsletter, e così via. Ma se poi ci guardiamo indietro e ci domandiamo: "Avevo davvero bisogno di quel servizio?", la risposta potrebbe non essere così scontata. Ed è così che, alla fine, ci ritroviamo interi indirizzi mail in disuso con 2000+ messaggi mai letti e che mai leggeremo (se non è il vostro caso, avete appena risparmiato 90% del tempo richiesto).  

Sembra paradossale, ma se abbiamo ancora accesso a quelle mail più simili a un cestino, è tempo di ringraziare la nostra pigrizia. Perché è proprio grazie a quei rifiuti che possiamo ricostruire filo per segno la nostra storia online e, grazie al GDPR - il regolamento europeo per la protezione dei dati -, andare su quei siti e cancellarli definitivamente. Perché? Per due motivi:
- ogni sito in meno equivale a una possibilità in meno che i nostri dati vengano venduti o rubati
- lo stesso motivo per cui Marie Kondō sistema casa: l'ordine mentale e la più facile gestione del tutto, per una vita più serena

Infine, questo ci aiuterà a rivalutare l'iscrizione a futuri siti, consapevolizzandoci un po' di più su cosa voglia dire effettivamente iscriversi, su quali dati vogliamo fornire, ma soprattutto su cosa voglia dire *dis*iscriversi.

## Cosa fare

Iniziamo col *come* fare: piano piano. Questo capitolo richiede tempo e sforzi, e fare tutto in fretta non porterà ad altro se non a lasciare perdere dopo un paio di giorni. Per esempio, ci si possono dedicare 10 minuti al giorno.  

### Occorrente
- foglietto delle password
- eventuale mail-spazzatura
- qualcuno di cui ci fidiamo con conoscenze sufficienti di internet se non siamo pratici

Prima di procedere, un'avvertenza: la cancellazione del profilo non funziona allo stesso modo per tutti i siti. Dato che non possiamo occuparci di ogni singolo sito (che è un po' come voler catalogare tutti i granelli di un pugno di sabbia), è qui che il "qualcuno di cui ci fidiamo" viene in nostro aiuto, se non siamo pratici. Più in basso abbiamo comunque elencato le varie tipologie di cancellazione. 

### Preparativi

Come prima cosa ritiriamo fuori il famoso foglietto del capitolo delle password: se vi ricordate, vi avevamo detto di ignorare i siti di cui non vi importava più ma di tenerli segnati. Questo perché ora saranno i primi siti a fare ciao ciao.  
Oltre al foglietto, recuperiamo la nostra mail-spazzatura, entriamo e diamo un occhio alla posta in arrivo. Troveremo posta da più siti e quello che dobbiamo fare per ogni singolo sito è chiederci: "Mi serve/uso/mi ricordo di questo sito?". Se la risposta è no, quel profilo può sparire. Niente sensi di colpa, niente forzarvi con "provo a recuperarne l'uso": se non l'avete usato fino ad ora, è perché non ne avevate bisogno. Ricordiamoci che l'idea è *togliere*, non aggiungere.

### Come cancellare un profilo
- **tasto "Cancella account"**: questo è il caso più fortunato in quanto più breve. Basta accedere al sito e andare a cercare il tasto nelle impostazioni del profilo. Solitamente è scritto in piccolo, dato che è l'ultima cosa che (giustamente) i proprietari vorrebbero che faceste.
- **contatto mail/ticket**: se non trovate nessun tasto, cercate su internet "come cancellare account *nomesito*". Forse c'era e non l'avevate visto, o forse scoprirete che vogliono essere contattati via mail. Dovrete ovviamente usare la mail che avete usato per la registrazione. Siate concisi, tipo

> Buongiorno,  
vorrei il mio profilo e i miei dati cancellati dal vostro database. Il nome utente è *mettete-il-nome-utente* e ho usato questa mail per la registrazione. Sì, sono sicuro/a della scelta.  
Cordiali saluti

Ci metteranno circa un giorno per rispondervi, e potrebbe andarvi bene che accettino subito, oppure...
- **mail di conferma**: oppure vi chiederanno se siete sicuri della scelta. In quel caso rispondetegli confermando di nuovo. Potrebbero inoltre chiedervi dei dati per verificare che siete veramente voi, per esempio quando avete creato il profilo, data di nascita e via dicendo. In casi rarissimi vi danno direttamente un modulo online da compilare per la cancellazione. Fatelo e aspettate un'eventuale mail di conferma
- **chiamata**: questo è il livello "perfido". In alcuni casi la mail non c'è e siete obbligati a chiamare. Come, per esempio, nel caso dell'EA (casa videoludica).
- **newsletter**: può capitare invece di non essere registrati a dei siti ma solo alla loro newsletter (o a entrambi). Per cancellarvi dalla newsletter, apritene una e in fondo troverete il link per cancellarsi (in inglese "Unsubscribe"). Potreste dover seguire un altro paio di passaggi, come selezionare un motivo per il quale vi state cancellando, ma niente di che.

### All'opera
Ora non vi resta che darci sotto con le cancellazioni. Iniziate da quelle inutili del foglietto, e tirateci sopra una bella riga quando siete sicuri che il profilo è stato rimosso. Per esempio, infatti, quando contattate un sito/app via mail, non viene cancellato in automatico: dovete aspettare la risposta di conferma. Prima di allora, non datelo per rimosso.  

Quando passerete ai siti della posta, di certo non potrete depennare lo schermo. Tuttavia, una volta confermata la cancellazione, potete usare lo strumento di ricerca della posta, cercare il sito da cui vi siete cancellati, e cestinarne tutte le mail. Fidatevi che vedere quel "posta in arrivo" ridursi sempre di più è anch'esso soddisfacente.  

Inoltre, se nei siti avete messo dati personali, come ulteriore protezione potreste cambiare quei dati prima di far sparire il profilo. Non sappiamo dirvi quanto influisca, ma dato che richiede 2 secondi tanto vale provare.  

### Siti ostici
Questa mini-sezione è per farvi un'idea dei siti più ostici. Oltre l'EA annunciata prima, abbiamo:
- giochi online. La maggior parte delle compagnie vi chiederà conferme nel dettaglio prima di cancellarvi
- Twitch. Nel profilo non appare nessun bottone per cancellare il profilo né si riceve risposta ai ticket. Questo perché il collegamento per cancellare il profilo è nella normativa sulla privacy: https://www.twitch.tv/user/delete_account


E infine, eccoci qui (probabilmente state sbirciando e non avete ancora iniziato, ma d'altronde è sempre meglio leggere fino in fondo). Se tutto va "secondo i piani", vi ritroverete iscritti a neanche una decina di siti e/o newsletter. Magari vi ritroverete addirittura a non aver neanche più bisogno della mail-spazzatura, ma di questo ne parleremo più avanti (Livello 2 e 3). Concentrandoci invece su quello che è stato fatto qui, ormai dovreste aver iniziato a capire come, seppur talvolta con fatica, possiamo esercitare il controllo su quella tanto sbandierata privacy che sembrava prima così distante. A capire, grazie a strumenti legislativi che mezzo mondo ci invidia, quanto possiamo effettivamente esercitare i nostri diritti, rifiutandoci di prendere parte ai giochi di potere nelle mani di pochi super-ricchi (siano essi privati a capo di aziende, o politici a capo di Stati). Il tutto, partendo anche da delle banali mail.  

Se questo capitolo non vi ha fermato, la strada torna ora in discesa. È tempo di introdurre il Livello 2, ed è tempo di farlo partendo dai biscotti.  

[❤️ Dicci la tua opinione su PrivaSì](https://sondaggi.eticadigitale.org/index.php?r=survey/index&sid=174788&lang=it)

[Torna al percorso](../../indice)  

## Appendice

[^1]: [Marie Kondō su Wikipedia](https://it.wikipedia.org/wiki/Marie_Kond%C5%8D)
