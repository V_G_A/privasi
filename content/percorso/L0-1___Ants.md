---
weight: 2
---

# Formiche in una teca

È difficile parlare di qualcosa che non si vede. Quindi [queste](https://myactivity.google.com/myactivity) sono le vostre attività Google e [questa](https://www.google.com/maps/timeline) la cronologia dei vostri spostamenti, se non avete negato il permesso al GPS.  

Per capire la portata di un fenomeno quale la perdita al giorno d'oggi degli spazi personali, l'operazione è semplice: moltiplicate le informazioni che ha raccolto Google su di voi per ogni grosso nome tecnologico usato dalla gran parte della popolazione mondiale (Facebook, Amazon, Microsoft, Huawei ecc), e moltiplicate di nuovo per ogni persona sulla faccia della Terra con una connessione internet. Otterrete un numero così grande che, esattamente quanto i soldi dei miliardari dietro a queste aziende, è umanamente inimmaginabile (si consideri che per capire le ricchezze di Jeff Bezos è stato fatto un sito a sé stante[^1]). Dobbiamo ora fare uno sforzo mentale, per capire cosa ciò comporta.

## 1. Informazione è potere
Immaginate di vivere in uno Stato che non conosce l'elettricità: niente frigo, niente comunicazione istantanea a chilometri di distanza, niente dispositivi per salvare vite (pacemaker, defibrillatore ecc). Immaginate ora che esista un altro Stato che l'eletticrità invece l'ha scoperta. Che rapporto ci sarà tra il vostro Stato e quest'ultimo? Magari è pacifista, vi rispetta e vuole condividere con voi il funzionamento dell'elettricità per far sì che possiate fare cose come surgelare il cibo... o più realisticamente vuole assoggettarvi, trasformandovi in forza lavoro sottopagata senza diritti, abusando delle vostre terre e dei vostri corpi; perché, in poche parole, ha il coltello dalla parte del manico.  
  
Per quanto banale, chi è al potere conosce a pieno il valore dell'informazione: persone come Edward Snowden hanno rinunciato alla propria libertà per rivelare informazioni, e altre come Khashoggi hanno brutalmente pagato con la vita[^2]. Non è infatti raro che alcuni Stati dittatoriali isolino interi territori dal resto del mondo per evitare che certe notizie fuoriescano (o entrino). Un caso recente è l'India con la regione del Kashmir[^3], ma troviamo da anni a questa parte casi come la Cina con la Grande Muraglia Digitale[^4], o la Corea del Nord col Kwangmyong[^5]. E questo non vale, si intende, solo per internet: si pensi per esempio al sopprimere la libertà di stampa, che permette di mantenere la stretta al potere tramite la distorsione delle notizie - quindi dell'informazione in mano ai cittadini.  

Immaginate ora di avere in mano le preferenze sessuali, politiche, gli umori, le relazioni, le mode, i sogni, le paure, i segreti di miliardi di persone. E che quei miliardi di persone non abbiano nulla di quello che avete voi. Se l'informazione è quindi potere, quanto potere avreste? Se voleste manipolarli, se ne accorgerebbero? Se voleste condurre degli studi su come reagiscono a certi contenuti piuttosto che ad altri, lo saprebbero? Se voleste influenzare un'elezione elettorale che modificherebbe gli assi di potere di un'intera nazione, lo capirebbero? La storia ci insegna[^6] che la risposta, purtroppo, è no.

## 2. Simbiosi
Il secondo punto da chiarire è che, al contrario di quel che si pensa, la rete non è né immateriale né apolitica. Compagnie come Google, Amazon e Apple rispondono al governo statunitense tanto quanto Huawei, AliBaba e TikTok rispondono a quello cinese. Internet non è un mondo distaccato dal mondo reale, e questo è diventato più chiaro all'opinione pubblica negli ultimi anni: si pensi per esempio a Trump quando decise di bandire TikTok dagli Stati Uniti perché sosteneva che la Cina lo usasse per spiare gli statunitensi[^7]. O pensate a come la tecnologia Rekognition di Amazon venga usata sulla frontiera Messico - Stati Uniti per fermare l'immigrazione[^8]. Ancora, pensate a come Google sia stata nel 2017 la compagnia tecnologica ad aver fatto più lobbismo in assoluto sul governo statunitense[^9]. O, per cambiare Stato, pensate a come TikTok cerchi di fare pulizia etnica eliminando gli utenti cinesi che parlano cantonese piuttosto che mandarino[^10]. Nello scacchiere geopolitico, questi spazi apparentemente innocenti e neutrali, sono quindi pedine tanto quanto il resto. Per l'esattezza, sono pedine con enorme potere potenziale, e con spazi di manovra molto più ampi di uno Stato fisico: anche questa è una cosa risaputa da chi è al potere, avendo creato tra Stati e certe compagnie delle situazioni di simbiosi (USA) o comunque di dipendenza (Cina). Si veda come esempio il conflitto israelo-palestinese, che ha visto sparire "per errore" diversi contenuti in supporto della Palestina su piattaforme statunitensi (Facebook, Instagram, Twitter)[^11], o WeChat che non recapitava i messaggi contenenti parole relative a un evento buddista condotto dal Dalai Lama (considerato un terrorista in Cina)[^12].

![L0-1_pic0.png](../../images/L0-1_pic0.png)  
*potere digitale nel 2012: il sito più visitato per singolo Stato[^13]*

---

Per concludere, avremmo potuto parlare di pubblicità mirata che segue le persone in giro per la rete, ma il discorso sarebbe stato limitativo - e alcuni non avrebbero visto il problema, tutt'altro. La pubblicità è solo quello che noi vediamo e che permette a certe aziende di fare soldi (e c'è comunque da sfatare il mito che quella mirata sia indispensabile, come dimostra un giornale nederlandese[^14]); tuttavia, la macchina che la produce può fare molto di più, e possiede un potere che nessuna persona dovrebbe mai avere. PrivaSì nasce quindi sì come percorso per riprendere il controllo della propria sfera privata online, ma nasce anche per togliere un chicco di controllo a questi behemoth cibernetici. Certo, seguire una guida di autodifesa non è un'azione sufficiente per fermare l'avanzata senza controllo di una sorveglianza di massa legalizzata: servono leggi, strumenti, persone. Tuttavia è un ottimo strumento dal quale partire per sensibilizzarsi, per fare informazione e, ancora più importante, per iniziare a dire "basta". Basta di essere un numero, basta di abbracciare l'idea che ormai non ci sia più nulla da fare (cosa che alle aziende fa molto comodo farci credere). Basta di essere formiche in una teca, e ricominciare invece a essere quello che si vuole, davanti a chi si vuole.  

  
[Livello 1: "Rimozione Consenso"](../../percorso/l1-0___activity-deletion/)  

[Torna al percorso](../../indice/)  

[❤️ Dicci la tua opinione su PrivaSì](https://sondaggi.eticadigitale.org/index.php?r=survey/index&sid=174788&lang=it)

## Appendice
[^1]: https://mkorostoff.github.io/1-pixel-wealth/, Aprite e scorrete verso destra  
[^2]: https://it.wikipedia.org/wiki/Edward_Snowden, https://it.wikipedia.org/wiki/Jamal_Khashoggi  
[^3]: Valigia Blu, [*Kashmir, la più lunga chiusura di Internet mai imposta in una democrazia: “Una violazione della libertà di espressione”*](https://www.valigiablu.it/kashmir-internet-bloccato/), Valigia Blu, 2020  
[^4]: Etica Digitale, [*Chi guarda i cinesi? Il Partito*](https://eticadigitale.org/2020/01/20/chi-guarda-i-cinesi-il-partito/), Etica Digitale, 20 gennaio 2020  
[^5]: https://it.wikipedia.org/wiki/Kwangmyong  
[^6]: Si vedano per esempio Cambridge Analytica (ne parleremo in seguito) e gli studi di Facebook sugli utenti senza che ne fossero consapevoli - sia per quanto riguarda [l'umore](https://www.theguardian.com/technology/2014/jun/30/facebook-emotion-study-breached-ethical-guidelines-researchers-say) che per quanto riguarda lo spingere (o meno) [a votare](https://www.scientificamerican.com/article/facebook-experiment-found-to-boost-us-voter-turnout/); si vedano anche le minacce velate di Facebook per rendere certi studi nascosti all'opinione pubblica come documenta Zuboff nel capitolo *Falli Ballare* de *Il capitalismo della sorveglianza* (Milano, 2019, LUISS)  
[^7]: Nd, [*TikTok, perché Trump vuole bloccarlo e Microsoft vuole comprarlo*](https://www.corriere.it/tecnologia/20_agosto_01/tiktok-perche-trump-vuole-bloccarlo-microsoft-vuole-comprarlo-03f6a0b6-d3c6-11ea-9996-35f9c99cd908.shtml), Corriere della Sera, 1 agosto 2020  
[^8]: Laperruque Jake, [*Amazon Pushes ICE to Buy Its Face Recognition Surveillance Tech*](www.thedailybeast.com/amazon-pushes-ice-to-buy-its-face-recognition-surveillance-tech), Daily Beast, 23 ottobre 2018; Dickey Megan Rose, [*On Prime Day, Amazon workers and immigrants’ rights organizations are protesting*](https://techcrunch.com/2019/07/15/amazon-prime-day-protest/), TechCrunch  
[^9]: Abramson Alana, [*Google Spent Millions More Than its Rivals Lobbying Politicians Last Year*](https://time.com/5116226/google-lobbying-2017/), Time, 24 gennaio 2018  
[^10]: Borak Masha, [*China’s version of TikTok suspends users for speaking Cantonese *](https://www.abacusnews.com/culture/chinas-version-tiktok-suspends-users-speaking-cantonese/article/3078138), South China Morning Post, 3 aprile 2020  
[^11]: Ingram Mathew, [*Social networks accused of censoring Palestinian content*](https://www.cjr.org/the_media_today/social-networks-accused-of-censoring-palestinian-content.php), Columbia Journalism Review, 19 maggio 2021  
[^12]: Crete-Nishihata M., Knockel J., Ruan L., [*Tibetans blocked from Kalachakra at borders and on WeChat*](https://citizenlab.ca/2017/01/tibetans-blocked-from-kalachakra-at-borders-and-on-wechat/), The Citizen Lab, 10 gennaio 2017  
[^13]: Adiklis Vytautas, https://visual.ly/community/infographic/technology/world-map-dominating-websites, 2012  
[^14]: Edelman Gilad, [*Can Killing Cookies Save Journalism?*](https://www.wired.com/story/can-killing-cookies-save-journalism/), Wired, 5 agosto 2020  

