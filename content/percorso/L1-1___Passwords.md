---
weight: 3
---

# Password 101

> Cosa rende una password una buona password? E dove conviene custodirle per tenerle al sicuro da occhi indiscreti? Vediamolo insieme

## In parole semplici
Immaginiamo di avere tante chiavi che aprono tante porte diverse: una per la porta di casa, una per la cantina, una per la cassetta della posta, il garage, la macchina, la porta dell'ufficio ecc.

Immaginiamo ora invece tante copie della stessa chiave, che ha la capacità di aprire tutte le cose elencate prima. Come se le serrature fossero tutte uguali. 
  
Cosa succede quando perdiamo una chiave? Nella peggiore delle ipotesi, nel primo esempio dobbiamo sostituire una serratura. Nel secondo, invece, dobbiamo sostituire *tutte* le serrature. Prima regola per una buona password: avere tante password.  
Se pensate che una password non sia importante quanto la chiave di casa vostra, provate a pensare a cosa succederebbe se qualche malintenzionato entrasse anche solo in un vostro profilo social: potrebbe cancellare tutti i vostri contenuti, spacciarsi per voi, pubblicare contenuti vietati dal regolamento, vedere i vostri dati sensibili come il numero di telefono e, il danno maggiore, leggere e salvare tutte le vostre chat private. Non proprio cose da poco. 

In questo capitolo vediamo insieme più modi per generare delle buone password e persino ingannare chi dovesse riuscire ad accedervi.

## Cosa fare
> ATTENZIONE: dato che si parla di password e carte di credito, si invita a non seguire queste procedure con persone di cui non vi fidate nei dintorni

### Rimozione password da internet
#### Google
Per prima cosa, sbarazziamoci di eventuali password salvate sul profilo Google andando [qui](https://passwords.google.com/). Se non ne abbiamo, ottimo; in caso contrario, invece, armiamoci di *carta e penna* e clicchiamo su un qualsiasi sito nella lista.  Per questioni di sicurezza, Google ci chiederà la nostra password Google. Inseriamola e ci troveremo in una pagina simile  

![L1-0_pic0.png](../../images/L1-1_pic0.png)

La password è nascosta, quindi per leggerla premiamo sull'icona dell'occhio barrato. Una volta resa visibile, scriviamo sul foglietto:
1. nome sito
2. nome utente
3. password

Dopodiché, premiamo su Elimina. Ripetiamo questo passaggio per *tutti* i siti, nessuno escluso. È di fondamentale importanza tenere traccia di ogni sito per ciò che vedremo in futuro, quindi __conserviamo con cura il foglietto__.

Inoltre, ancora più importante delle password, assicuriamoci di non avere i dati della nostra carta di credito salvati su Google andando [qui](https://pay.google.com/payments/home#paymentMethods) e, se utilizziamo Chrome, andando [qui](chrome://settings/payments). Nel primo caso basta premere su "rimuovi", nel secondo caso disabilitate l'opzione "Salva e compila i metodi di pagamento".

#### Browser internet
Tutti i browser moderni si offrono di salvare le nostre password quando le inseriamo, ma farlo è sconsigliato per diversi motivi[^1]. Se usate Chrome e siete da PC le trovate [qui](chrome://settings/passwords). Se usate Firefox [qui](about:logins). In generale, nelle impostazioni del browser sotto una voce _password_ o simili. 
Prima di rimuoverle, segniamole su un foglietto come abbiamo fatto con Google qui sopra.

## Regole per una buona password
Vediamo ora, invece, le regole da seguire per una password sicura:
* Almeno 8 caratteri di lunghezza
* Utilizzare lettere maiuscole, minuscole, numeri, e (volendo) simboli
* Non utilizzare variazioni del nostro nome, cognome, data di nascita, squadra preferita, eccetera
* Non utilizzare una parola comune (ad esempio "ciao")
* Cercare di usare sempre password diverse
* Una password formata da 3-5 parole è più facile da ricordare e più difficile da violare rispetto a una singola parola scritta in modo strano
* Se rima è più facile da ricordare

Quindi, per esempio, "FaggioPorcospinoNatura5" è più sicura di "P4$$w#rxD".  
Inoltre, prima di utilizzare una password, assicuriamoci che questa non sia già nota in qualche database. Per farlo, c'è il sito [Have I Been Pwned](https://haveibeenpwned.com/Passwords) (la password non verrà inviata né memorizzata dal sito, non preoccupatevi).  

### Dove tenere le password
Iniziamo da dove **NON** tenerle:
* su un post-it incollato sullo schermo (molto comune negli uffici)
* su un documento sul PC
* nelle note del telefono

Per il resto, le scelte sono 3:

#### Cartaceo
Per chi ha meno dimestichezza con la tecnologia, il posto più sicuro è un foglio di carta. Un quaderno, un’agenda, un foglio volante, poco importa: l'importante è tenerlo al sicuro. Annotate quindi tutti i siti ottenuti dai passaggi precedenti con i rispettivi nomi utente, ma aspettate ancora un attimo a segnare le password.

Vi mostriamo ora un modo col quale, anche se una persona dovesse avere accesso al vostro foglietto, non riuscirebbe a combinare nulla. In gergo tecnico si chiama "pepare" (*peppering*).

Pepare una password vuol dire ~~metterci il pepe~~ aggiungere una parolina in più[^2], sempre uguale, in tutte le password SENZA scriverla sul foglio. 
Per esempio, mettiamo che la nostra parola sia "SEDIA". Il foglio recita così (useremo password SCONSIGLIATE solo a scopo illustrativo):
> Facebook: ciao123  
Twitter: melanzana6  
Instagram: girandola90  

Se qualcuno prova ad accedere usando queste password, non ci riesce. Perché? Perché le vere password sono
> Facebook: ciao123SEDIA  
Twitter: melanzana6SEDIA  
Instagram: girandola90SEDIA

E il vantaggio è che dobbiamo ricordarci una sola parola. Ovviamente potevamo aggiungerla all'inizio (SEDIAciao123 ecc.), dopo due caratteri (ciSEDIAao123) ecc., a preferenza nostra.

Arrivati qui, sapete come avere password sicure, dove tenerle e addirittura come camuffarle. Vi ricordate il foglio dove vi era stato detto di non scrivere ancora le password? Perfetto, ora scrivete di fianco a ogni sito le NUOVE password che vorreste mettere su quel sito (camuffandole). Se proprio non v'interessa nulla di un certo sito e non avete sopra dati sensibili, ignoratelo ma NON rimuovetelo (a meno che non vogliate cancellare subito il profilo)

#### In locale con KeePassXC
Invece, se avete dimestichezza con il PC potete usare [KeePassXC](https://keepassxc.org/) per PC o [KeePassDX](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free) per Android che è un gestore di password locale. Le password, in poche parole, vengono salvate su un file sul vostro dispositivo. Il tutto protetto da una password generale, così dovete ricordarvene solo una. 

Segue una GIF con dimostrazione della creazione di un nuovo database, generazione della sua password tramite il tasto col simbolo del dado ⚂ (ricordate di salvarla da qualche parte!) e creazione di una voce al suo interno.

![L1-1_gif0.gif](../../images/L1-1_gif0.gif)

#### Sulla nuvola con Bitwarden
Con [Bitwarden](https://bitwarden.com/) potete barattare un po' di sicurezza (perchè le password vengono caricate sui loro server) per più comodità sulla nuvola. È probabilmente il più affidabile sulla piazza (per chiarirci: anni luce in avanti rispetto alle password salvate su Chrome). Vi basta farvi un profilo e, anche qui, ricordare una password unica per accedere a tutte le altre, da dove volete. C'è sia la versione gratuita che quella a pagamento.

Con i gestori di password come Keepass o Bitwarden, non è necessario pepare le password, possiamo farcele generare automaticamente. Con Bitwarden è semplice: basta premere sul tasto 🗘 che trovate nell'app o nelle schede delle credenziali[^3].

Altro consiglio: abilitate l'**auto-completamento** sul telefono. Questo permetterà all'applicazione di mostrare un tasto "riempi automaticamente con Bitwarden" sotto le classiche caselle _nome utente_ e _password_ di siti e app. Per farlo basta andare nelle impostazioni di Bitwarden e dentro _Servizi di riempimento automatico_ attivare tutte e tre le opzioni, concedendo i permessi richiesti[^4].
  
Bitwarden è disponibile per tutti i dispositivi e sul PC vi consigliamo di metterlo anche come estensione sul browser che usate così da accedere facilmente ai vari siti[^5]. Se siete utenti *davvero* avanzati, una soluzione più privata è ospitare Vaultwarden sul proprio server (basta un Raspberry Pi o simile).[^6]

### Conclusione

Dulcis in fundo, bisogna cambiare le vecchie password con quelle nuove. Per farlo dovete ovviamente connettervi ai vari siti e modificarle dalle loro impostazioni; questa operazione può richiedere *più* o meno tempo a seconda di quante password dovete modificare, ma non demordete: fatto una volta, non dovrete più preoccuparvene :)

### Sicurezza extra: autenticazione a due fattori
Avete presente le O-Key della banca che generavano un codice di 6 cifre prima di ogni azione importante? Stiamo parlando esattamente della stessa cosa.  
Molti siti hanno implementato l'autenticazione a due fattori (o *verifica in due passaggi*) ed è caldamente consigliata per quei siti sensibili che volete "blindare", come la posta (se non usate Gmail) o un social network (abbiamo visto prima cosa può succedere).  
Per inviarci il codice come faceva la O-Key, ci sono due opzioni: via SMS e via app sul telefono. Cercate di evitare gli SMS (comporta dare il numero al servizio) e usate invece app come [Authenticator Pro](https://play.google.com/store/apps/details?id=me.jmh.authenticatorpro). Non possiamo purtroppo illustrare come impostare l'autenticazione per ogni sito in circolazione, ma vi basta cercare "2FA nomesito" per trovare facilmente qualche guida.  

Infine eccoci qui: quando si diceva di prendere il percorso con calma, ci si riferiva soprattutto a capitoli come questo. Una persona con 30 siti importanti ci metterà ovviamente più tempo rispetto a chi di importanti ne ha 5. Tuttavia, provate a vederla da un'altra prospettiva: avete davvero bisogno di tutti quei siti? Ci torneremo tra qualche punto, quando inizieremo a fare pulizia.

[Torna al percorso](../../indice)  



## Appendice
[^1]: Salvare le password nel browser non è una buona pratica perché è un software complesso quindi suscettibile a diversi problemi di sicurezza che potrebbero essere sfruttati da estensioni o siti malevoli. Inoltre, spesso i browser salvano le password sul disco senza crittografarle, quindi in seguito a un furto del dispositivo sarebbe più facile recuperarle. Banalmente se il computer è utilizzato in condivisione con altre persone, queste potrebbero vedere le password e accedere facilmente ai servizi a cui sono collegate. [Qui più info](https://www.makeuseof.com/reasons-shouldnt-use-browsers-password-manager/)
[^2]: https://bitwarden.com/help/generator/
[^3]: Codice su [Github](https://github.com/dani-garcia/vaultwarden) - Vaultwarden è una riscrittura del codice del server di Bitwarden che lo rende più facilmente ospitabile su server domestici e meno potenti. È una soluzione più privata perchè vi permette di non dipendere da un server esterno.
[^4]: https://bitwarden.com/help/auto-fill-android/
[^5]: https://bitwarden.com/help/auto-fill-browser/
[^6]: in verità consiste nell'aggiungere caratteri in più, che siano lettere, numeri o simboli (tipo "9Qò("). Tuttavia, si è voluto semplificare per non rendere la frase troppo complessa.
