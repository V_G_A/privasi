---
# Title: "Conversazioni sensibili pt.1: Smart TV e assistenti vocali (Internet delle Cose)"
weight: 5
---

# Conversazioni sensibili pt.1: Smart TV e assistenti vocali (Internet delle Cose)

> L'Internet delle Cose punta a conquistare la nostra vita: ogni apparecchio è "intelligente", comunica con i suoi simili e ci fa vivere tra mille agi; in tutto ciò, un'assistente vocale si prende cura di noi. Ma a quale prezzo?

## In parole semplici 
Una smart TV è una TV connessa a internet, solitamente dotata di un microfono. Se ne si ha una, vi invitiamo a dare un occhio al manuale delle istruzioni; perché, cercando bene, è probabile che troverete un'avvertenza sul *non* parlare di argomenti sensibili nei pressi del televisore (o il rimando alla policy sul sito internet per non scriverlo esplicitamente). Considerando che casa dovrebbe essere lo spazio più intimo di una persona e che la gente si autocensura quando sa di essere monitorata fino alla paranoia ("Ho detto qualcosa di male? Come si comporterebbe un innocente? Posso parlare di questa cosa? Devo far finta di niente? Esagero il mio comportamento per far capire che sono una brava persona? E ma se poi invece...?"), vi lasciamo riflettere su quanto sia ansiogeno che un elettrodomestico suggerisca di star attenti a ciò che si dice nella propria sfera privata. La si pensi come guidare e avere una volante della polizia dietro: non state facendo niente di male, ma vi sentireste rilassati come se dietro ci fosse una qualsiasi altra macchina?  
Quando ciò fece scalpore, nel 2015 Samsung si giustificò dicendo che lo faceva per migliorare la tecnologia[^1]. Tuttavia, nel 2017 WikiLeaks dimostrò come la CIA potesse sfruttare una falla proprio in quei televisori per monitorare le persone grazie al microfono incorporato nella TV[^2].  

Se avessimo una TV non connessa a internet, per manometterla qualcuno dovrebbe fisicamente venire in casa nostra e metterci le mani. Avrebbe, quindi, ostacoli fisici (il cancello, gli occhi dei vicini, la porta e il tempo richiesto per pasticciare con la TV rimanendo nell'abitazione). Al contrario, con internet, tutto questo non è necessario: può essere manomesso in venti modi diversi a chilometri di distanza. E la cosa più grave è che, una volta trovata, la falla può essere sfruttata su tutti gli altri dispositivi uguali al nostro sparsi per il mondo. Questo vuol dire che *chiunque* con abbastanza conoscenze può metterci mano: che sia il vicino con manie di controllo, il governo o un gruppo di ragazzi annoiati poco importa, ciò che importa è che si hanno dei mezzi in casa che possono essere trasformati in cimici a costo zero.  
Ovviamente non vale solo per le TV, ma per qualsiasi cosa con un microfono, telecamera e/o sensori connessi a internet (ragion per cui nel percorso di PrivaSì si spiega passo passo come rendere sempre più sicuro il telefono).

Lasciamo ora da parte la sicurezza. Produttori di smart TV come Roku TV, LG, Vizio e la già citata Samsung raccolgono dati su ciò che vediamo e da dove ci connettiamo, usando quei dati per guadagnare ulteriormente[^3]: di nuovo, un'attività per se stessi smette di essere solo per se stessi e viene condivisa con un'azienda (dopo che la si è pagata), che la condivide poi con terzi. Stesso discorso si applica agli assistenti vocali (Alexa, Cortana ecc.) che, c'è da ricordarsi, per potersi attivare con un comando vocale (generalmente il loro nome), hanno bisogno di stare *sempre* in ascolto. Una profilazione, quindi, non più sulla singola persona, ma sull'intero nucleo familiare.  

C'è inoltre chi potrebbe vedere quest'ultimi come un compagno con cui parlare e combattere la solitudine. Tuttavia, non è con una macchina né *attraverso* una macchina che si sconfigge la solitudine: per esempio, intervistando gli streamer più influenti di Twitch (gente che fa dirette di ore e ore chiusa in camera, guadagnando interi stipendi e con tanti fan coi quali dialoga via chat nel mentre), è risultato come questi avessero problemi quali solitudine, ansia e depressione a causa del loro lavoro che li tiene costretti tra quattro mura per la maggior parte della giornata, senza altre persone se non quelle dietro allo schermo[^4]. O, se si vuole un esempio più pratico, si pensi al vissuto durante la pandemia quando non si poteva uscire di casa.  

Con questo, attenzione, non si vuole demonizzare l'Internet delle Cose in toto: al crepuscolo del XX secolo, infatti, le domotiche (le case "intelligenti") stavano venendo progettate con l'intento di dare il pieno controllo dei dati a chi risiedeva sotto il loro tetto[^5]. Il dialogare fra strumenti era ideato per una rete locale, e non invece per comunicare con un server centrale di qualche azienda su internet. Se le istruzioni che date ad Alexa di base non lasciassero casa vostra, in altre parole, avreste effettivamente un'assistente vocale che vi rispetta. Al contrario, queste vengono mandate a un server centrale (in questo caso a quello di Amazon) per venire sbobinate e garantire all'azienda più potere. *Questo* è il vero problema: la mancanza di controllo su uno strumento che permea i nostri spazi più intimi.

## Cosa fare
Il primo passo, brutale quanto necessario, è il seguente: sbarazzatevi di questi strumenti proprietari e non acquistatene di nuovi. Casa vostra è, appunto, vostra. E la sfera privata anche.  

Qualche suggerimento:  
- Se questi mezzi vi fanno sentire capiti e meno soli, provate a riflettere sul fatto che siano assistenti, fatti per assecondarvi. Se non avete amici, se state passando un brutto periodo, ricordatevi che siamo animali *sociali*. Provate a riallacciare i rapporti con qualcuno o a prendere parte a qualche gruppo di volontariato nella vostra città. In generale, non abbiate paura a chiedere una mano a figure professionali come uno psicologo, perché a tutti può capitare di sentirsi soli in un'epoca dove la percezione della solitudine è via via aumentata[^6].
- Se pensate di non volerli buttare perché ormai ci avete speso dei soldi, vedetela così: terreste in casa un diffusore per ambienti dopo aver scoperto che quella marca nuoce alla salute?
- Se invece pensate che questi mezzi siano ormai essenziali nella vostra vita, o se ne necessitate per motivi fisiologici (si pensi per esempio a chi ha delle disabilità), non tutto è perduto. Continuate a leggere

Il secondo passo, per chi non può comunque farne a meno e ha abbastanza capacità tecniche, è quello di sostituire i vecchi strumenti proprietari con strumenti liberi che si propongono di fare esattamente quello che si immaginava nel 2000: tenere i dati delle macchine dentro le mura di casa.  
Si parla di "capacità tecniche" perché si è ancora in un territorio dove - perlomeno in italiano - manca ancora la comunicazione necessaria per portare queste tecnologie al grande pubblico. Trattarle qui nel dettaglio renderebbe il capitolo molto lungo, onde per cui preferiamo rimandare il lettore incuriosito ai siti di questi prodotti, lasciandolo esplorare.  

I grandi progetti sono essenzialmente due: [Mycroft](https://mycroft.ai/) quando si parla di assistenti vocali da mettere in casa, e [Home Assistant](https://www.home-assistant.io/) quando si parla di domotica.  

Invitiamo infine a non confondere comodità e necessità con la pigrizia: viene da sé che questi strumenti rendono la nostra vita più facile, ma c'è da tracciare una linea tra questo e l'uomo di cui narrava Seneca ne *La brevità della vita*, ormai così pigro da venir imboccato e diventato incapace di alzarsi da solo.  

[Torna al percorso](../../indice/)  

## Appendice
[^1]: De Souza Ryan, [Be careful of what you say in front our Smart TV, warns Samsung](https://www.hackread.com/samsung-smart-tv-listening-conversations/), HackRead, 2016  
[^2]: Brandom Russel, [The CIA is hacking Samsung Smart TVs, according to WikiLeaks docs](https://www.theverge.com/2017/3/7/14841556/wikileaks-cia-hacking-documents-ios-android-samsung), The Verge, 2017  
[^3]: Fowler Geoffrey A. [You watch TV. Your TV watches back.](https://www.washingtonpost.com/technology/2019/09/18/you-watch-tv-your-tv-watches-back/), The Washington Post, 2019  
[^4]: Glink, [The Dark Side of Streaming](https://yewtu.be/watch?v=Iz81XKFOANI) (video), 2017  
[^5]: Si sta parlando del progetto Aware Home realizzato dall'Istituto di Tecnologia della Georgia. Vedasi Kidd Cory D. *et al.*, [The Aware Home: A Living Laboratory for Ubiquitous Computing Research](https://www.cc.gatech.edu/fce/ahri/publications/cobuild99_final.PDF), 1999  
[^6]: Kurzgesagt, [Loneliness](https://yewtu.be/watch?v=n3Xv_g3g-mA) (video), 2019
