---
# Title: "LibreOffice"
weight: 8
--- 

# ~~Microsoft office~~ Libreoffice

> Windows è il sistema operativo più utilizzato e, con esso, la sua suite Office. Un'inchiesta del governo olandese tuttavia, ne ha dimostrato l'alta invasività

## In parole semplici  

LibreOffice è una variante *libera* (come abbiamo visto quando abbiamo definito il *software libero*) dei principali strumenti del pacchetto Office di Microsoft (Word, Excel e PowerPoint). Quest'ultimo è infatti stato trovato a inviare telemetria non disattivabile ai suoi server[^1], come per esempio i destinatari delle mail, frasi sistemate col correttore, frasi tradotte e, più in generale, metadati (quelli visti nel [capitolo di WhatsApp](../../percorso/l1-4___signal/)). Per fare un paragone, Windows 10 in sé invia 1.200 tipi di dati a 10 team diversi. La suite Office 365 invece ne invia *25.000*. Più di 20 volte tanto, rendendola a tutti gli effetti un pozzo di petrolio di raccolta dati, a pagamento.  

Comportamenti simili hanno spinto la Germania a bandire dalle scuole questa suite nel luglio 2019, reputandola un pericolo per la privacy[^2]. Oltre le scuole, anche altre istituzioni pubbliche come l'associazione federale IT tedesca che si occupa dei servizi municipali si è mostrata preoccupata in merito, dicendo di star prendendo in considerazione alternative libere e che bisognerebbe riservare dei fondi per questi strumenti. Soprattutto se si considera che nel 2018 in Germania hanno speso circa 73 milioni di euro per le licenze Microsoft[^3]. Al contrario, l'Italia marcia in direzione opposta, scendendo sempre più a patti con Microsoft[^4].

Sul lato tecnico, c'è chi potrebbe dire che a livello di prestazioni LibreOffice non raggiunga quelle di Microsoft. Ma, a meno che non abbiate file da centinaia e centinaia di MB l'uno, il programma non dà alcun problema. Sottolineiamo inoltre che, oltre ad avere un formato tutto suo, apre tutti i file Word, Excel ecc. fatti col pacchetto Microsoft, quindi non ci sono problemi di compatibilità. Infine, potreste aver sentito parlare di un'altra alternativa libera chiamata OpenOffice, ma dato che non viene aggiornata da anni, ve la sconsigliamo.  

Insomma: se volete evitare l'ennesimo programma di raccolta dati e al tempo stesso risparmiare soldi, l'alternativa esiste ed è perfettamente legale.  


## Cosa fare  
Beh... 
- scaricatelo dal sito ufficiale => https://it.libreoffice.org/download/download/
- installatelo
- ~~profit~~ fine

Inoltre, se siete i responsabili di un qualsivoglia ufficio, potreste considerare l'idea di passare a LibreOffice "business" (più info [qui](https://it.libreoffice.org/download/libreoffice-per-aziende/)), risparmiando sulle spese delle licenze. E di quei soldi risparmiati, riservarne una fetta per supportare gli sviluppatori del programma, così che possano renderlo sempre più prestante e risparmiare a tutti telemetria invadente: https://it.libreoffice.org/donazioni/
Un guadagno per entrambi.

[Torna al percorso](../../indice)   

## Appendice
[^1]: Cimpanu Catalin, [Dutch government report says Microsoft Office telemetry collection breaks GDPR](https://www.zdnet.com/article/dutch-government-report-says-microsoft-office-telemetry-collection-breaks-gdpr/), ZDNet, 14 novembre 2018  
[^2]: Schaer Cathrin, [Microsoft Office 365: Banned in German schools over privacy fears](https://www.zdnet.com/article/microsoft-office-365-banned-in-german-schools-over-privacy-fears/), ZDNet, 12 luglio 2019  
[^3]: Krempl Stefan, [73 Millionen Euro pro Jahr: Kosten für Microsoft-Lizenzen beim Bund steigen](https://www.heise.de/newsticker/meldung/73-Millionen-Euro-pro-Jahr-Kosten-fuer-Microsoft-Lizenzen-beim-Bund-steigen-4466370.html), Heise, 9 luglio 2019  
[^4]: Angius Raffaele, Zorloni Luca, [30mila caselle email della scuola italiana passano a Microsoft](https://www.wired.it/internet/web/2020/07/06/microsoft-scuola/), Wired, 6 luglio 2020; Adozione Microsoft Teams per le assemblee di istituto nelle scuole https://usr.istruzione.lombardia.gov.it/piattaforma-teams-microsoft/, 9 dicembre 2020; Pinassi Michele, [Roma ha speso 6.300.000€ + IVA per le licenze Microsoft](https://www.zerozone.it/politica-societa/roma-ha-speso-6-300-000e-iva-per-le-licenze-microsoft/18823), Zerozone, 4 febbraio 2021; Cristiano Ghidotti, [PA: Leonardo e Microsoft per la digitalizzazione](https://www.punto-informatico.it/leonardo-microsoft-digitalizzazione-pa-infrastrutture/), Punto Informatico, 26 maggio 2021
