---
#Title: "Personale: Tor, cipolle digitali"
weight: 21

_build:
  render: never
---

# Personale: Tor, cipolle digitali

> Tor: quella rete dove la modalità incognito è davvero in incognito. Alla scoperta dello spazio privato

# In parole semplici

Se volete sentirvi l'hacker hollywoodiano, questo è il momento giusto: Tor nasce infatti come progetto militare per condividere informazioni in maniera sicura attraverso la rete, ed è poi stato portato avanti dalla no profit Tor Project in maniera del tutto FOSS[^1]. Solitamente, esso viene abbinato a cose come il *deep web* dove assoldare assassini, comprare schiavi e altre mitiche leggende che lo fan sembrare l'anticamera dell'inferno, ma ci si scorda volutamente di menzionare che è uno strumento essenziale per evadere la censura dei paesi autoritari (lo usa persino la BBC[^2]), e che chi tira su imperi della droga come Silk Road prima o poi viene beccato (i pagamenti sono ben o male rintracciabili)[^3]. Il compito di Tor, perciò, è quello di garantire l'anonimato di chi lo usa.  
  
Per capire come funziona, partiamo dal suo nome: Tor sta per *The Onion Router*, il router cipolla. È una cipolla perché è fatto a strati - 3 per l'esattezza - e se vi ricordate il discorso nel capitolo dell'esodo di "un braccio è buono, due sono meglio", questa è esattamente la stessa cosa applicata all'anonimato.  
  
Ognuno di questi strati è infatti un computer, che qualcuno ha messo a disposizione per chi vuole navigare tramite Tor. Quando ci connettiamo, Tor pesca 3 computer a caso tra tutti i computer disponibili (attualmente qualche migliaio), che avranno il compito di tenere al sicuro l'identità di chi c'è davvero dietro (noi). Per capirlo meglio, immaginiamo di voler inviare un pacco alla famiglia Wikipedia. Al posto di spedire il pacco direttamente al loro indirizzo - che di conseguenza svelerebbe anche dove abitiamo - lo consegniamo a un postino misterioso che, al posto di portarlo dove specificato, lo consegna a un centro di smistamento casuale. Nel centro di smistamento, viene rimosso il nostro indirizzo come mittente, messo quello del centro e inviato a un altro centro casuale. E da lì, una volta ricambiato l'indirizzo, a un altro ancora. Ecco, questi sono i 3 strati.  
L'ultimo centro spedirà infine il pacco alla famiglia Wikipedia che, contenta della sorpresa, decide di inviarci subito un pacco a sua volta. Come? Beh, il nostro indirizzo non lo sa. L'unica cosa che può fare è darlo al postino ancora lì davanti, cosicché lo riporti all'ultimo centro di smistamento. Il centro, che ha i registri degli ordini, lo invierà a quello prima, che lo invierà a quello prima, che... ha il nostro indirizzo e lo invierà a noi!  
Può sembrare buffo, ma è così che funziona internet, a pacchi (o meglio, a pacchetti). La rete non è altro che un esercito di postini digitali che fanno avanti e indietro per farci caricare il sito che abbiamo richiesto, il gioco online che abbiamo scaricato, il messaggio che abbiamo inviato. Tor non fa altro che aggiungere più passaggi nel mezzo, barattando una minor velocità per una maggior anonimia.  
  
A proposito di *maggior* anonimia, una domanda che salta fuori spesso è "quindi Tor è completamente anonimo?" e la risposta breve è no; è tuttavia ciò che si avvicina di più all'anonimato completo, in quanto richiede ingenti risorse governative per farlo vacillare (e di nuovo: se aveste un governo alle calcagna, non sareste qui a leggere). Un'altra domanda è "meglio VPN o Tor?", e a questo rimandiamo il lettore a consultare l'infinità di articoli e video fatti sull'argomento in giro per la rete - in quanto servirebbe un capitolo a sé stante.    
  
Ma alla fine di tutto ciò, quando usare Tor, e perché? Per il quando, ecco, pensate a quelle volte che aprite internet in modalità incognito, e sostituelo con Tor. Usatelo per masturbarvi, per esempio (i siti porno sono ricolmi di traccianti pubblicitari). O se abitate in regimi autoritari. Se la lentezza non vi turba[^4], usatelo in generale quanto possibile se non dovete entrare con qualche profilo, o comunque usatelo per ciò che ritenete più privato per voi - come appunto la sfera sessuale - se la rete è privata. NON usatelo su reti pubbliche (come quella scolastica, soprattutto se siete così cretini da inviare minacce di bombe perché volete saltare un esame[^5]) perché anche se non si può sapere COSA fate su Tor, è comunque possibile sapere che lo state usando. 

## Cosa fare

Per navigare sulla rete Tor, abbiamo bisogno del suo browser apposito. Chiamato, per l'appunto, Tor Browser.

#### Telefono: F-Droid

Prima di tutto bisogna attivare il "negozio" da dove scaricare il browser di Tor. Per farlo, apriamo F-Droid, andiamo nelle impostazioni situate in basso a destra e premiamo su `repository`. Attiviamo `Guardian Project Official Releases` e F-Droid si aggiornerà inserendo i contenuti del nuovo "negozio".  
Ora ci basterà cercare `Tor Browser` (senza `Alpha`, quella è una versione per sviluppatori) e scaricarlo come tutte le altre app.  

Installato e aperto, ci chiederà al primo avvio quale livello di sicurezza vogliamo utilizzare: `Standard`, `Sicuro` e `Il più sicuro`. Vi sconsigliamo caldamente di navigare in modalità Standard, perché tra le tre è quella che adotta meno precauzioni. La sicura è, al contrario, un ottimo compromesso tra il mantenere l'anonimato e visualizzare correttamente la stragrande maggioranza dei siti senza avere problemi nella navigazione. Infine, se vi sentite particolarmente smanettoni e non volete correre il benché minimo rischio di essere rintracciati per qualsivoglia motivo - a scapito di trovarsi molti siti pressoché inutilizzabili - allora optate per l'ultima, "Il più sicuro".  

Passando alle varie impostazioni ed estensioni... beh, è molto semplice: non toccate nulla.
Questo perché il miglior modo di essere anonimi è confondersi nella massa. Quello che di base fa Tor non è altro che far apparire i propri utenti come persone tutte uguali. Se noi decidessimo di metterci un cappello (l'estensione in più) in una folla di persone che il cappello non ce l'ha, risulteremmo subito riconoscibili. Ecco: stesso discorso :D  

A questo punto siamo pronti a navigare. Il motore di ricerca è già impostato su DuckDuckGo, e la nostra cronologia verrà cancellata ogni volta che chiuderemo Tor (se si vuole essere sicuri di averlo fatto, basta premere il menu con i tre puntini e schiacciare su `Esci`). Basterà premere su `Connetti`, attendere una manciata di secondi, e ci ritroveremo dentro la cipolla digitale.  

#### Computer

**Windows e Mac**: recatevi sulla [pagina di download del sito ufficiale](https://www.torproject.org/download/), scaricatelo e installatelo dove meglio credete come un normale programma  
**Linux**: consigliamo caldamente di scaricarlo dai repository ufficiali (il pacchetto è chiamato `torbrowser-launcher`) in modo che venga aggiornato in automatico dal vostro gestore di pacchetti

Una volta aperto e datogli il tempo di connettersi alla rete, clicchiamo sull'icona dello scudo in alto a destra e poi su `Impostazioni di sicurezza avanzate`. Anche qui, tra le tre opzioni che vi appariranno vi consigliamo come compromesso `Sicuro`. E come per il telefono, non avremo bisogno di toccare nient'altro.  
L'unico accorgimento è quello di evitare di cambiare la dimensione della finestra, perché anche quello può contribuire a rendervi meno anonimi[^6].

Sempre in alto a destra troviamo l'icona della scopa, che ci permetterà di ripartire da zero cancellando le tracce precedenti (e reimpostando la dimensione della finestra, nel caso l'avessimo rimpicciolita o ingrandita). Infine, come da telefono, possiamo uscire manualmente tramite il tasto "Esci".

#### Accorgimenti vari

Ora che siamo pronti per navigare, teniamo sempre a mente che:

* se accediamo ad un profilo che utilizziamo nel web tradizionale, possiamo essere deanonimizzati
* se le nostre ricerche o la nostra navigazione saranno riconducibili alla nostra persona, possiamo essere deanonimizzati
* se per iscriverci ad un servizio (social, forum) utilizzeremo un nome o un nome utente che possono essere ricondotti alla nostra navigazione nel web tradizionale, possiamo essere deanonimizzati
* se vogliamo restare anonimi non potremo usare le email dei servizi in chiaro, dovremo utilizzare email che non abbiamo mai utilizzato altrove, ottenute solo tramite la rete Tor e senza fornire alcun dato personale che sia anch'esso riconducibile alla nostra identità. Lo stesso vale per ogni altra informazione o profilo in nostro possesso che intendiamo utilizzare su Tor
* il nodo di uscita (l'ultimo centro di smistamento, per intenderci) può vedere tutto quello che succede sui siti che non iniziano con HTTP**S**

Arrivati a questo punto, la nostra navigazione internet è passata dall'essere una fabbrica non stop di dati e comportamenti a un machiavellico labirinto dove starci dietro è tutto fuorché facile. Manca giusto un ultimo capitolo per finire l'opera: comprendere e usare uBlock Origin con precisione chirurgica.  
  
[Torna al percorso](../../indice)

## Appendice
[^1]: https://www.torproject.org/  
[^2]: Nd, [BBC News launches 'dark web' Tor mirror](https://www.bbc.com/news/technology-50150981), BBC, 2019  
[^3]: Carola Frediani, [#Cybercrime](https://www.hoeplieditore.it/scheda-libro/frediani-carola/cybercrime-9788820389208-6127.html), Milano, 2019, HOEPLI; si veda il capitolo *Vita, morte e delirio dei mercati neri*  
[^4]: preparatevi a venire inondati da ReCAPTCHA come se piovesse per dimostrare che non siete un robot  
[^5]: Nd, [Harvard student Eldo Kim charged in final-exam bomb hoax](https://edition.cnn.com/2013/12/17/justice/massachusetts-harvard-hoax/), CNN, 2013  
[^6]: Per chi se ne intende: sì, c'è l'avanzo del letterboxing, ma se JS è abilitato ritorna un'entropia diversa. Meglio essere cauti
