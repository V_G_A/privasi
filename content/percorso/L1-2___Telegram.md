---
# Title: "Conversazioni di tutti i giorni:  ̶M̶e̶s̶s̶e̶n̶g̶er Telegram" #NON TOCCARE MESSENGER
weight: 4 
---

# Conversazioni di tutti i giorni: ~~Messenger~~ Telegram

> È nato prima l'uovo o la gallina? Allo stesso modo, è meglio Whatsapp o Telegram? C'è gente che ha litigato per molto meno. La risposta è, semplicemente, che son due app diverse e avrebbe più senso paragonare Whatsapp con app come Signal (come vedremo nel prossimo capitolo) o Wire. Quindi, se proprio vogliamo fare un paragone, prendiamo un'altra app molto famosa: Facebook Messenger. È meglio Messenger o Telegram? 

## In parole semplici

Partiamo dalla cosa più banale: Messenger appartiene a Facebook.  

Vi sarà sicuramente capitato in questi ultimi anni di sentire che il social di Zuckerberg stesse avendo problemi con la privacy. Il motivo è semplice: Facebook, oltre a condividere lo stesso modello di business di Google, si è dimostrato irresponsabile nel tutelare i dati raccolti. Questi dati sono infatti finiti nelle mani di aziende come Cambridge Analytica, che li ha poi utilizzati per manipolare elezioni politiche tramite annunci pubblicitari mirati e personalizzati[^1].
  
Parlando nello specifico di Messenger, un'inchiesta del New York Times ha dimostrato come Facebook abbia dato a Spotify, Netflix e alla Royal Bank of Canada il permesso di leggere, scrivere e cancellare i messaggi privati degli utenti[^2]. Al di fuori di uno scarica barile dove nessuno sapeva niente, il problema resta: qualcuno aveva accesso alle conversazioni private di qualsiasi utente, con inoltre la possibilità di modificarle e immedesimarsi in loro. Non importa se poi lo hanno fatto o no (potremmo speculare all'infinito senza avere una risposta), importa che era possibile farlo.  

Sempre parlando di terze parti col permesso di guardare la chat, fino a inizio agosto 2019 gli audio inviati su Messenger erano affidati a terzi per essere trascritti, senza il benché minimo avviso all'utente[^3]. In parole semplici, c'erano persone pagate per ascoltare gli audio inviati in chat. Ironico, considerato che tre mesi prima Zuckerberg affermò: "Il futuro è privato"[^4]. E ci si potrebbe anche chiedere se avrebbero smesso di farlo se non li avessero scoperti (che è quello che è successo).  

Questo apre una riflessione sulla fiducia: come possiamo verificare che un programma faccia ciò che dice? Beh, dobbiamo avere la possibilità di guardarci dentro. O meglio, noi magari non ci capiremo granché, ma chi ha studiato sì, e loro potranno garantire per gli altri. Tutto questo si traduce nella prima parola chiave del percorso: **software libero** (chiamato infelicemente anche *open source*[^5]).  

Il software libero in verità non è solo la libertà di guardare dentro a un programma: è anche la libertà di poterlo usare senza limitazioni, crearne delle copie, poterle modificare, ridistribuirle[^6]. È insomma qualcosa fatto dalle persone per le persone, che niente ha a che vedere con le meccaniche di monopolio del mercato - e che anzi garantisce più sicurezza, perché dispone di molti più occhi che possono controllarlo, perdipiù non influenzati da interessi economici.

Per quanto infatti Zuckerberg (e chiunque altro) possa garantirci che l'app fa solo X e Y, o che il futuro è privato, l'unica scelta che abbiamo è quella di credergli o meno. In questo caso specifico dovremmo fidarci di qualcuno che ha lasciato trapelare i dati di 87 *milioni* d'utenti con Cambridge Analytica, che ha fatto della profilazione il suo modello di business e che ha posizioni discutibili sull'etica di narrare la verità con gli annunci pubblicitari - anche se possono minare la democrazia di un paese -, purché permettano di far fare soldi all'azienda[^7].  

Quindi qual è un'alternativa libera per Messenger? Intanto capiamone la sua funzione: possiamo dire che Messenger sia un'app per chattare con persone alle quali non vogliamo dare il numero. Tra le alternative più famose ci sono quindi WeChat, LINE, Telegram e Viber; ma tra queste, ce n'è solo una che è software libero: Telegram ([qui il suo codice sorgente](https://telegram.org/apps#source-code)).  
Per essere precisi, solo una parte è libera, ma in linea di massima vi basterà evitare argomenti sensibili per non avere problemi.

## Cosa fare
Scarichiamo l'app di Telegram dal negozio virtuale del nostro telefono e, una volta completato lo scaricamento, apriamola. Partirà un'installazione guidata dove ci verrà chiesto il nostro numero di telefono e un nome utente. Fatto ciò, siamo pronti per partire.

Inoltre, dato che non vogliamo mostrare il nostro numero alle persone a cui abbiamo dato il nick, clicchiamo le tre lineette in alto a sinistra e nel menù a scomparsa premiamo su "Impostazioni".  
Lì andiamo su Privacy e Sicurezza e, premendo su Numero di telefono, mettiamo "Nessuno". Le altre opzioni invece impostatele a vostra discrezione.  

Torniamo poi indietro alle Impostazioni e, se vogliamo impostare un'immagine profilo, clicchiamo sull'icona della fotocamera. Potremo scegliere se scattarla sul momento o caricarla dalla galleria.  
![L1-2_pic0.png](../../images/L1-2_pic0.png)

### Gruppo Telegram di Etica Digitale
Tra le tante funzioni di Telegram, c'è un sistema di ricerca per trovare persone, gruppi e canali (gruppi a senso unico dove si segue ciò che scrive il proprietario). Anche Etica Digitale ha sia un gruppo che un canale, dove siete tutti i benvenuti: il primo è un luogo d'incontro dove riflettere su tutto ciò che concerne l'etica digitale (e per chiedere aiuto se non capite qui qualcosa), mentre il secondo è un canale di notizie dal mondo - in italiano - riguardo appunto i temi del gruppo.  
Per trovarli, torniamo alla schermata principale e premiamo sulla lente d'ingrandimento.  
Si aprirà in automatico la barra di ricerca e lì scriviamo "etica digitale". Come primo risultato dovreste avere sia il gruppo (`@eticadigitale`, come da immagine) che il canale (`@eticadigitalechannel`)  

![L1-2_pic1.png](../../images/L1-2_pic1.png)

Per entrare premeteci su: in entrambi, in basso noterete le scritte per unirvi. Premete anche su quelle e il gioco è fatto.  

### Regolamento
Si ricorda che, prima di scrivere nel gruppo, è opportuno leggere il regolamento. Lo trovate nel "messaggio fissato" del canale, ovvero nella parte alta dello schermo come da immagine. Se ci premete, vi porta al messaggio vero e proprio.  
  
![L1-2_pic2.png](../../images/L1-2_pic2.png)

Prima di lasciarvi pasticciare con le varie funzioni di Telegram (non nel gruppo :))) ), è opportuno ricordare che il capitolo si chiama "Conversazioni di tutti i giorni". Questo perché vi **s**consigliamo di condividere informazioni troppo sensibili sull'app, in quanto parte del codice di Telegram (il lato server) non è consultabile - anche se c'è da dire che il fondatore non ha mai dato ragione di dubitare della sua integrità. Per quel tipo di conversazioni, useremo Signal (che vedremo tra un paio di capitoli).  

### Versione Desktop
Se volete usarlo dal PC senza controllare ogni volta il telefono, Telegram ha anche una comodissima versione Desktop: la potete scaricare da [qui](https://desktop.telegram.org/)  

### Rimuovere Messenger
Ultimo ma non meno importante, disinstalliamo Messenger dal nostro telefono.  
Forse starete pensando che i vostri amici/familiari/colleghi/clienti sono lì e che non volete/potete lasciarli, ma rifletteteci un attimo: perché farsi del male se c'è una soluzione? Prima di tutto, se non potete proprio farne a meno, potete comunque continuare a usare la chat di Facebook dal computer. Questo è ottimo se avete clienti/datori di lavoro con i quali dovete rimanere in contatto e ai quali non volete dare il numero: perché fidatevi, la vostra sanità mentale non vuole che queste persone possano raggiungervi a qualsiasi ora del giorno o della notte sul telefono. Sul serio.    
Per gli amici e familiari il problema invece non si pone, perché si suppone abbiate i loro numeri e li sentiate già su app come WhatsApp o Signal.  
Tuttavia, c'è una soluzione ancora più semplice: perché non spiegare a queste persone quello che avete letto qui in alto? La privacy non è un insieme chiuso, funziona soltanto se anche le persone intorno a noi ne sono consapevoli. Che alla fine è il motivo per cui queste pagine esistono.  
E perché poi, una volta che saranno su Telegram, il problema avrà smesso di esistere.  

[Torna al percorso](../../indice/)  

## Appendice
[^1]: Consigliamo la visione di [The Great Hack](https://www.thegreathack.com/) per approfondire l'argomento  
[^2]: Dance Gabriel J.X., LaForgia Michael, Confessore Nicholas, [As Facebook Raised a Privacy Wall, It Carved an Opening for Tech Giants](https://www.nytimes.com/2018/12/18/technology/facebook-privacy.html), The New York Times, 2018  
[^3]: Frier Sarah, [Facebook Paid Contractors to Transcribe Users’ Audio Chats](https://www.bloomberg.com/news/articles/2019-08-13/facebook-paid-hundreds-of-contractors-to-transcribe-users-audio), Bloomberg, 2019  
[^4]: Statt Nick, [Facebook CEO Mark Zuckerberg says the ‘future is private’](https://www.theverge.com/2019/4/30/18524188/facebook-f8-keynote-mark-zuckerberg-privacy-future-2019), The Verge, 2019  
[^5]: Stallman Richard, [Perché l'“Open Source” manca l'obiettivo del Software Libero](https://www.gnu.org/philosophy/open-source-misses-the-point.it.html). Inoltre, da un punto di vista linguistico, *open source* è un sinonimo che è stato assegnato successivamente con fini utilitaristici, per far sì che le aziende non storcessero la bocca a sentire *free* di *free software* - in inglese "free" vuol dire sia libero che gratis. Se è vero che in inglese c'è un problema di ambiguità, nelle lingue romanze questo non accade, ergo non dovremmo ridurre la lingua ai limiti dell'altra se tali limiti nella nostra non ci sono (e anzi l'altra sta cercando di usare "libre software" come alternativa). Così facendo si mantiene intatto l'aspetto filosofico che c'è dietro
[^6]: Si veda per una spiegazione completa: [https://www.gnu.org/philosophy/free-sw.it.html](https://www.gnu.org/philosophy/free-sw.it.html)
[^7]: Guardian News, ['So you won't take down lies?': Alexandria Ocasio-Cortez challenges Facebook CEO](https://yewtu.be/watch?v=8KFQx-mc2Ao) (video), 2019
