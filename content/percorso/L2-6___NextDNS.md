---
# Title: "Bloccare i traccianti di terze parti"
weight: 17

---

# Bloccare i traccianti di terze parti

> Abbiamo iniziato questo livello parlando dei cookie[^1] e di come queste "briciole" che lasciamo per la rete possono essere raccolte e usate per creare un profilo di cosa visitiamo e quindi chi siamo. Purtroppo, però, i cookie non sono l'unica tecnologia che viene usata per questo scopo.

## In parole semplici

Una delle funzionalità essenziali che hanno reso il WWW così popolare è la possibilità di avere parti di siti che vengono caricati all'interno di altri siti.

Per fare un esempio: il sito della regione Lombardia, una volta aperto, non carica soltanto i contenuti di regione.lombardia.it ma si collega anche ad altri siti, che gli forniscono strumenti o contenuti aggiuntivi. Questa funzione può sicuramente essere utile, ma può altresì essere usata per caricare contenuti il quale scopo è raccogliere dati sulla nostra navigazione. Chiamiamo tali contenuti *traccianti di terze parti*.[^2] I traccianti più comuni sono quelli di colossi come Google, Facebook e Amazon[^3], che in questo modo esercitano un controllo al di fuori dei loro spazi internet, tracciando un profilo su chi naviga ancora più preciso.

![Schermata del sito della Regione Lombardia](/../images/L2-6_pic0.png "Schermata del sito della Regione Lombardia")
*schermata del sito della Regione Lombardia, che contatta Google per il servizio di traduzione*

Non è finita qui: come abbiamo visto nel [capitolo su F-Droid](/percorso/l2-1___f-droid/#dimmi-che-app-sei-e-ti-dir%C3%B2-quanti-traccianti-hai), i traccianti esistono anche all'interno di molti programmi e applicazioni, non solo nei siti. Come possiamo, quindi, assicurarci di evitare la raccolta di informazioni invasiva anche sulle app? Se pensiamo a quelle che abbiamo installato sul telefono, infatti, si va dai giochini di poca importanza ad applicazioni come quelle della banca, sanitarie o amministrative, che gestiscono informazioni estremamente sensibili sul nostro conto.[^4]

uBlock Origin, l'estensione per browser con lo scudo rosso consigliata nel [capitolo sui browser](/percorso/l2-2___firefox/), blocca soltanto i siti visitati con il browser e quindi non può aiutarci. Uno strumento come NextDNS, invece, ci protegge da traccianti e pubblicità fastidiose bloccando **a livello di dispositivo** tutte le chiamate fatte dai programmi per raccogliere dati su di noi (e solo quelle; le app continueranno a funzionare tranquillamente).

## Cosa Fare

### Registrarsi

Registratevi su https://my.nextdns.io/signup

Seguite le istruzioni che trovate nella pagina `Installazione` e una volta fatto questo passiamo alla configurazione.

### Configurazione
1. Andate su https://my.nextdns.io > `Impostazioni` > `Log` > disabilitate la spunta `Abilita log`
2. Nella stessa pagina, andate nella sezione `Prestazioni` e abilitate tutte le spunte
3. Andate su https://my.nextdns.io > `Privacy` > `Liste di blocco` > Aggiungete una lista di blocco > aggiungete `OISD` , `1Hosts (Lite)` e `EasyList Italy` 
4. Nella stessa pagina, andate nella sezione `Protezione dal tracciamento nativa` > `Aggiungi` > aggiungete qualsiasi prodotto utilizziate. Ad esempio, se avete un computer Windows e un telefono Xiaomi, aggiungete le rispettive protezioni.

Il piano gratuito prevede un limite di 300.000 richieste al mese, tenete d'occhio la pagina `Statistiche` e se superate il numero di richieste, cosa poco probabile, considerate di usare più account gratuiti o di fare l'abbonamento.

Se utilizzate più account gratuiti ripetete la configurazione per ciascun account.

[Torna al percorso](../../indice)   

## Appendice
[^1]: [Cookie: briciole di internet](/percorso/l2-0___cookies/)
[^2]: "Terze parti" perchè sono esterni al servizio principale - https://spreadprivacy.com/how-do-hidden-web-trackers-put-my-privacy-at-risk/
[^3]: https://slayterdev.github.io/tracker-radar-wiki/
[^4]: Ad esempio, qui ci sono i traccianti di [PosteID](https://reports.exodus-privacy.eu.org/reports/posteitaliane.posteapp.appposteid/latest) (potete notare Google), [IO](https://reports.exodus-privacy.eu.org/en/reports/it.pagopa.io.app/latest/) (potete notare Facebook), [Intesa Sanpaolo Mobile](https://reports.exodus-privacy.eu.org/en/reports/com.latuabancaperandroid/latest/)
