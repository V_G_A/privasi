---
# Title: "Motori di ricerca: al di là di Google"
weight: 7
---

# Motori di ricerca: al di là di Google

> Nel 2000 venivano fatte circa 33 milioni di ricerche al giorno su Google. Nel 2019, le ricerche giornaliere erano stimate a ben 3.5 *miliardi* (3.500.000.000)[^1]. I motori di ricerca sono ormai, quindi, parte del nostro quotidiano tanto quanto lavarci la faccia e mangiare. Ed esattamente come nel mangiare dovremmo prestare attenzione a cosa mangiamo per evitare problemi fisici, nel cercare su internet dovremmo prestare attenzione a cosa usiamo per evitare manipolazioni mentali.

## In parole semplici

Mettiamo che venga scoperta una nuova pianta: Tizio dice che ha proprietà benefiche e per questo va coltivata, Caio dice che uccide le piante a lei vicina e per questo va estirpata. Chi ha ragione?  

Nel mondo di oggi, bombardati costantemente da informazioni (giornali, TV, telefono, computer), le risposte più realistiche sono due: o facciamo una ricerca su internet cliccando uno dei risultati in cima, oppure apriamo il primo collegamento che capita a tiro sul nostro social preferito. Ora la domanda è: l'articolo che avete aperto è davvero il primo che avreste trovato o è il primo che *il mezzo* (Google, il social ecc.) vi voleva far trovare?  

No, niente complotti: algoritmi. Vi siete mai chiesti perché i post di alcune persone che avete tra gli amici su Facebook non appaiono mai sulla vostra home? Perché, semplicemente, Facebook non ritiene rilevanti i loro post per voi. E come fa a dirlo? Conoscendo i vostri gusti e le vostre idee (nel 2015 gli bastavano circa 10 like per conoscervi più di un collega, 70 per gli amici, 150 per i parenti e 300 per i coniugi[^2]).  
In sintesi, se qualcuno condivide un articolo su come la pianta sia il male e Facebook già sa che avete messo mi piace su articoli che elogiano la pianta, non si prenderà il disturbo di farvelo apparire sulla home. Questo è *gravissimo*. È gravissimo perché isola le persone in bolle percettive che filtrano la realtà mostrando solo i contenuti che vogliono vedere, polarizzando e convincendo chi guarda che il mondo *deve* andare come dicono loro, perché sulla loro home di Facebook e sulle ricerche di Google non vedono altro. Ognuno, in altre parole, pensa di detenere la verità, e le cose che scorre ogni giorno sullo schermo del telefono ne sono l'apparente riprova.  

Stesso discorso vale per la manipolazione generale dei risultati quando cerchiamo qualcosa: nel 2014 un'inchiesta di Yelp ha dimostrato come Google manipolasse le ricerche degli hotel[^3], e Google stesso ammette di rimuovere i termini negativi nell'autocompilazione quando vogliamo cercare qualcosa. Prova del 9: scrivete "Hitler" nella barra di ricerca. Quanti termini "negativi" vi suggerisce (assassino, crimini, genocidio, guerra, morte ebrei ecc.)?  

Nel primo caso quello che sta facendo è abusare della sua posizione, mentre nel secondo è *fingere* che non esistano cose negative a questo mondo. Ma se persino gli psicologi ci spiegano che non dovremmo celare la morte ai bambini[^4], per quale ragione dovremmo nascondere le cose negative a noi adulti? Il mondo non è rosa e fiori e censurare ciò che non vogliamo sentire (vero o meno che sia) non lo farà andare via.  

Quindi come difendersi da certi strumenti che isolano gli individui nella loro verità personale, diventando una minaccia per il dialogo e il confronto? Beh... si cambia strumento. Al contrario di quello che si pensa, ci sono molti altri motori di ricerca là fuori, e di seguito vi mostreremo pro e contro dei più rilevanti.

E a proposito della pianta: *se* hanno entrambi ragione, basta coltivarla lontana dalle altre piante. Magia :)  

## Cosa fare
Come dicevamo ci sono vari motori di ricerca in circolazione. Quello che stiamo cercando è un motore che ci dia risultati pertinenti e che non ci usi per profilarci e/o portare acqua al suo mulino. Se cerchiamo X, deve essere X.  
Mettiamo le mani avanti dicendo che il motore di ricerca per eccellenza non esiste (che non vuol dire che siano tutti uguali, anzi), ed è per questo che ne abbiamo voluto consigliare più di uno: a seconda delle vostre necessità.  
Una cosa deve però essere chiara: qualsiasi cosa scegliate da qua sotto, sappiate che a livello di privacy è meglio di Google.

### DuckDuckGo

![L1-5_pic0.png](../../images/L1-5_pic0.png)

Compagnia statunitense con una papera come icona, DuckDuckGo è senza ombra di dubbio l'alternativa più diffusa: le sue ricerche sono pertinenti, è facile e comoda da usare, ha un'app tutta sua sia sul Play Store (Android) che App Store (iOS), è adottata da browser privati come Tor, e non censura. O meglio, propone 3 tipi di ricerca - disattivata, moderata, rigorosa - a seconda delle preferenze (per esempio una persona al lavoro vorrà evitare di trovarsi immagini porno digitando parole ambigue, utilizzando perlomeno la ricerca moderata).  

Se non volete niente di complicato e volete evitare l'ennesima profilazione, questa papera è perfetta. A parte l'app, il sito ufficiale è https://duckduckgo.com/ o - ancora più breve - https://duck.com.  
Una volta aperta da PC vi chiederà in automatico se volete installarla: cliccando su "Installa" (quella sotto è solo un'immagine a scopo illustrativo, non fa niente), da ora in poi qualsiasi cosa cercherete dalla barra di ricerca del vostro browser, sarà cercata con DuckDuckGo.  

![L1-5_pic1.png](../../images/L1-5_pic1.png)

Ha inoltre una comoda funzione chiamata *bangs*. I bang sono ricerche "speciali", che iniziano con un punto esclamativo, seguito da una parola chiave. Per esempio, cercando

```
!w pinguino
```

cercherà su Wikipedia "pinguino", senza passare dalla pagina dei risultati. Perché "w" è la parola chiave associata a Wikipedia. Qui una lista: https://duckduckgo.com/bang  
Si sottolinea tuttavia, che un bang non rende la ricerca privata, perché è come se steste aprendo Wikipedia e digitando "pinguino" nella loro barra.  

### Brave Search

![L1-5_pic2.png](../../images/L1-5_pic2.png)

Per il secondo consiglio si passa da un logo con una papera a uno con un leone: quello di Brave, azienda che ha sviluppato prima un browser e poi il motore di ricerca che potete provare qui: https://search.brave.com/. 

Facciamo un confronto: l'idea iniziale di alcuni motori privati come DuckDuckGo o Startpage è stata quella di fare da intermediari inoltrando le ricerche delle persone a Bing o Google ma ripulendola dalle informazioni personali collegate. Con Brave Search tutto ciò non è necessario perchè la sua caratteristica principale è che ha lavorato da subito a un proprio **indice indipendente**[^5].

Come funziona in breve: i risultati che vengono elencati quando si usa un motore di ricerca non vengono effettivamente cercati al momento della richiesta. Si sfruttano, invece, dei "ragni" virtuali che perlustrano in un altro momento tutta la ragnatela (non a caso si chiama _web_) di siti esistenti e li raccolgono e raggruppano per argomenti, parole chiave e altro; in una parola: li _indicizzano_.

È come quando in biblioteca chiedete se è presente un libro e la persona incaricata non controlla libro per libro su tutti gli scaffali ma ha già una lista in cui guardare e sapere su che scaffale si trova ciò che cercate.

Questo permette a Brave Search di non dipendere da multinazionali esterne che filtrano o manipolano i risultati e a noi di avere, a lungo termine, alternative indipendenti e private a Google e Bing.

Ultima chicca: mostra anche risultati da discussioni[^6] su forum o Reddit e supporta i _bang_ di DuckDuckGo. 

### Mojeek

![L1-5_pic3.svg](../../images/L1-5_pic3.svg)

Allo stesso modo del precedente, Mojeek è un motore di ricerca con indice indipendente attivo dal 2005. È forse l'unico progetto realmente indipendente perchè è stato uno dei primi ad applicare una politica contro la profilazione, non si è mai basato su Google o Bing ed è una realtà gestita da poche persone senza grosse multinazionali dietro, con sede fuori dagli USA. 

Al momento Mojeek è consigliato per un utilizzo avanzato perché a volte può mostrare risultati poco pertinenti (soprattutto in italiano), ma per ricerche specifiche può essere migliore di altri. Il sito è https://mojeek.com/ e se volete aiutare lo sviluppo potete lasciare un commento con l'apposito tasto spiegando come migliorare i risultati.

### SearXNG

![L1-5_pic2.svg](../../images/L1-5_pic2.svg)

Ancora più avanzato, potete ospitare SearXNG con le dovute precauzioni sul vostro server personale, scaricandolo dal loro [GitHub](https://github.com/searxng/searxng/), o usando un'istanza pubblica (affidabile) da https://searx.space/. Se non sapete di cosa stiamo parlando, vi *s*consigliamo questa opzione.  

### Motori meno consigliati

- **Qwant**: motore di ricerca dell'Unione Europea, riporta su un suo post del 2015 di avere i server forniti da Huawei[^7]. Huawei è una compagnia cinese che, come ogni compagnia cinese che vuole sopravvivere, *deve* aderire al Partito Comunista[^8]. Purtroppo, la Cina, la libertà e l'etica non vanno molto d'accordo, tanto che nel Paese sembra di vivere in un episodio di Black Mirror a causa del Sistema di Credito Sociale[^9] (e sembra che anche l'America ci stia facendo un pensierino[^10]). Non sappiamo se i server siano ancora in collaborazione con Huawei dato che l'ultima notizia è del 2017, ma è meglio prestare attenzione (l'unica cosa che si sa è che esiste una collaborazione tra Qwant e Huawei per installare Qwant come motore di ricerca predefinito nei telefoni di fattura orientale al posto del classico Google[^11], nient'altro).
- **Swisscows**: bunker svizzero dentro una montagna, tiene una linea di censura per via dei bambini sostenendo che internet dovrebbe essere "per tutta la famiglia"[^12]. Tuttavia, guardando la questione alla radice, il problema non dovrebbe essere censurare internet per i bambini, bensì tenerli lontani da internet fino a una certa età. Con la stessa logica sennò, non si potrebbero mandare in onda i TG fino alle 22 perché i bambini sono svegli. Inoltre, sappiamo tutti quali sono i siti più visitati sulla rete...
- **Ecosia**: per quanto piantare alberi a seconda di quante ricerche si facciano e ad vengano cliccati suoni una bella iniziativa, questo motore di ricerca verde lascia a desiderare sulla privacy: le ricerche vengono salvate permanentemente, condividono dati con terze parti e tracciano usando strumenti interni[^13]. Di conseguenza sarebbe meglio contribuire all'ambiente in altri modi più concreti ed evitare motori simili.  
- **StartPage**: StartPage sfrutta il motore di ricerca di Google eliminando quelle parti che gli permettono di tracciare gli utenti. Tuttavia, se i risultati di Google vengono manipolati e/o censurati, di conseguenza anche quelli di StartPage lo saranno. Inoltre, dopo il suo acquisto da parte di una compagnia pubblicitaria e le derivate preoccupazioni degli utenti, Startpage si è limitato a rispondere in modo vago per quanto riguarda le implicazioni sulla privacy[^14].

### Motori sconsigliati (ok, qua siamo a livelli Google)
- **Yandex**: un Google russo. Colleziona tutto e di più e li condivide con terze parti[^15].
- **Bing**: motore di ricerca di Microsoft. Colleziona dati e li condivide con terze parti[^16].
- **Yahoo**: passato sotto Verizon, compagnia telefonica americana che colleziona dati e li condivide con terze parti, anche il motore in sé dichiara di collezionarli e condividerli con esterni[^17].
  
[Torna al percorso](../../indice/)

## Appendice
[^1]: nd, [How many Google searches per day on average in 2019?](https://ardorseo.com/blog/how-many-google-searches-per-day-2019/), Ardor Seo, 2019  
[^2]: Youyou Wu, Kosinski Michal, Stillwell David, [Computer-based personality judgments are more accurate than those made by humans](https://www.pnas.org/content/112/4/1036), PNAS, 27 gennaio 2015
[^3]: Edwards Jim, [Here's The Evidence That Google's Search Results Are Horribly Biased](https://www.businessinsider.com/evidence-that-google-search-results-are-biased-2014-10?IR=T), Business Insider, 2014  
[^4]: nd, [“Coco”, i bambini e la morte](https://www.ilpost.it/2018/01/13/coco-parlare-di-morte-ai-bambini/), Il Post, 2018  
[^5]: https://brave.com/search-independence/
[^6]: https://brave.com/discussions-in-brave-search/
[^7]: http://web.archive.org/web/20210303061639/https://blog.qwant.com/qwant-rejoint-huawei-au-cebit-15-2/
[^8]: Yi-Zheng Lian, [China, the Party-Corporate Complex](https://www.nytimes.com/2017/02/12/opinion/china-the-party-corporate-complex.html), The New York Times, 2017  
[^9]: Perroni Marta, [La Cina come Black Mirror: entro il 2020 introdurrà un punteggio sociale](https://www.tpi.it/2018/03/20/cina-punteggio-sociale-black-mirror/), TPI News, 2019  
[^10]: Elgan Mike, [Uh-oh: Silicon Valley is building a Chinese-style social credit system](https://www.fastcompany.com/90394048/uh-oh-silicon-valley-is-building-a-chinese-style-social-credit-system), Fast Company, 2019
[^11]: https://betterweb.qwant.com/en/qwant-x-huawei-qwant-users-lose-nothing-huawei-users-gain-everything/
[^12]: https://swisscows.ch/it/about#family-friendly  
[^13]: https://www.reddit.com/r/privacy/comments/cvhfyw/noble_goals_but_ecosia_falls_short_in_their/  
[^14]: https://www.reddit.com/r/privacy/comments/di5rn3/startpage_is_now_owned_by_an_advertising_company/  
[^15]: https://yandex.com/legal/confidential/  
[^16]: https://privacy.microsoft.com/it-it/privacystatement  
[^17]: https://policies.oath.com/ie/it/oath/privacy/products/communications/index.html  
