---
# Title: "Introduzione: cosa me ne importa della privacy?"
weight: 1
---

# Introduzione: cosa me ne importa della privacy?

A parlare di privacy, si sfocia generalmente in:
- Non ho nulla da nascondere
- Tanto hanno già i miei dati
- In qualche modo li prendono comunque

Analizziamo dunque queste affermazioni.

## 1: Non ho nulla da nascondere
Se dovessimo trovare un sinonimo di questa frase, esso sarebbe "Posso mostrare tutto di me". Eppure in questo mondo le persone chiudono la macchina, la porta di casa, tengono le distanze agli sportelli della banca, evitano che qualcuno sbirci il PIN della carta, si bisbigliano cose all'orecchio, non hanno rapporti intimi in pubblico e hanno un codice di sblocco per il telefono. Qualcuno potrebbe pensare che molti di questi aspetti siano rilegati più alla sicurezza che all'intimità, ma se un telefono o un computer contengono chat intime, foto di documenti, prenotazioni varie o anche semplici diari, non è questa intimità? Possiamo capire molto delle persone anche con una rapida occhiata al loro telefono.  

A volte invece si dice di "non avere nulla da nascondere" quasi per assicurare che non si è dei criminali; come se la sfera privata fosse cosa da delinquenti, e non invece parte della nostra natura. Nel privato si può infatti sbagliare senza essere giudicati, capire di più se stessi, conoscere il proprio corpo, ridere, piangere, sognare, danzare come idioti, cantare a squarciagola, suonare senza prendere una nota; fare qualsiasi cosa senza la costante idea di essere osservati. Questo renderà la cosa personale, propria e di chi abbiamo deciso di circondarci; la rende importante, perché non è di tutti.


## 2: Tanto hanno già i miei dati
Un abuso non giustifica un ulteriore abuso. "Tanto hanno già i miei dati" non implica che siano autorizzati a continuare ad averne, esattamente come una situazione di violenza non implica che non ci si debba opporre. L'identità delle persone cambia nel tempo, non rimane la stessa: cambiano le relazioni, il carattere, la visione del mondo, gli interessi, ma anche cose fisiche come il corpo, il domicilio o il numero di telefono. Quello che si conosce oggi, quindi, non è uguale a quello che sarà domani. E seppur non possiamo modificare il passato, nulla ci vieta di operare sul futuro. 
 
## 3: In qualche modo li prendono comunque
Prima di tutto, a meno che non si sia ricercati da chissà quale ente governativo, questo è falso. E se così fosse, non sareste qui a leggere queste righe. Secondo, questa affermazione deriva da un problema che va avanti da decenni, ovvero la mancanza di istruzione digitale: non sono molte le persone che saprebbero descrivere come funziona un computer, la differenza tra un protocollo HTTP e HTTPS, o più banalmente la funzione di un cookie di cui tanto si sente parlare. Quello che la maggior parte delle persone sa è come pubblicare su Facebook, rispondere a un messaggio su Whatsapp, fare una ricerca su internet e, talvolta, mandare una mail o scaricare un'app. Ma utilizzare uno strumento non implica sapere *come* funziona. Che è quello che, con parole semplici, si propone di fare questo percorso: colmare le lacune digitali che si riscontrano persino a livello politico, qui in Italia come altrove.  

Se poi si parla invece di sicurezza, "In qualche modo li prendono comunque" equivale a dire che non c'è bisogno di chiudere la porta di casa a chiave perché, tanto, se vogliono i ladri entrano lo stesso. Chiudere la porta diminuisce drasticamente la possibilità di intrusione, e nessuno affermerebbe che, dato che i grimaldelli esistono, non ne vale la pena. Ci deve essere una giusta misura. La struttura del mondo digitale può essere difficile da comprendere, ma bisogna fare questo sforzo mentale per capire che, essendo ormai estensione della persona, un'intrusione nella sfera digitale è in grado di causare tanti danni quanto quelli nella sfera fisica.  

Questo percorso, attenzione, non vuole essere un manuale sulla paranoia, guidando all'installazione di una super blindata di ultima generazione con venti serrature e chiavistelli (che anzi, alimenterà ancora di più la paranoia, fino a una spirale ossessiva che causerà solo dolore e disagio nell'individuo), bensì sul voler far riappropriare la persona dei suoi spazi digitali, dandole la possibilità di scegliere cosa, quando, come, dove e con chi condividere parti di sé. In altre parole, un percorso sul diritto di essere padroni e padrone della propria individualità e dei propri spazi nel mondo digitale.  

[Continua: "Formiche in una teca"](../../percorso/l0-1___ants/)  

[Torna al percorso](../../indice)
