# Architettura del progetto

## Rami
Il progetto si suddivide in rami:
0. `master`: branch dove le modifiche vengono implementate e rese pubbliche
1. `nome-modifica`: branch specifico per l'introduzione di una modifica

## Struttura delle cartelle
```
.
├── ARCHITETTURA.md -> Questo file
├── .gitignore -> Elenco dei files che git deve ignorare
├── .gitlab-ci.yml -> File per la creazione delle GitLab Pages
├── .gitmodules -> Sottomoduli di git
├── config.yaml -> File di configurazione del sito
├── content -> Cartella che contiene i contenuti del sito
│   ├── FAQ.md -> Domande frequenti relative al Percorso
│   ├── images -> Immagini del Percorso
│   ├── indice.md -> Indice del Percorso
│   └── percorso -> Cartella contenente i testi dei capitoli della guida
├── ISTRUZIONI.md -> Istruzioni per la collaborazione
├── layouts -> Cartella contenente i layout HTML
│   ├── index.html -> La pagina principale (landing page) del sito
│   └── partials
│       └── footer.html -> Piè di pagina modificato del sito
#perche` "modificato?"
├── LICENSE -> Licenza del sito
├── README.md -> Informazioni relative al progetto su GitLab
├── static -> Contiene favicon e loghi del sito
#conviene spiegare cosa sia una "favicon"
└── themes -> Cartella dei temi
    └── indigo -> Sottomodulo del tema utilizzato nel sito
```